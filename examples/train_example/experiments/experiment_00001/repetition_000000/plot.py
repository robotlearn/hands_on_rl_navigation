#!/usr/bin/env python
#  type: ignore

# TODO: plot without changing layout of tables
# TODO: don't plot trajectory to goal if the agent di not reach the goal

from hands_on_rl_navigation.env.mpi_env.mpi_env import MpiEnv
import exputils as eu
from hands_on_rl_navigation.networks import *
from hands_on_rl_navigation.networks.utils import *
from hands_on_rl_navigation.agent.sac.sac import *
from hands_on_rl_navigation.utils.plotter_utils import *
from run import config


def policy_eval(config_run, **kwargs):
    seed = 5005

    # Using the config from the training
    # config_env=config.env
    # config_env.show_gui_for_testing = True

    # Using an other config

    config_env = eu.AttrDict(
        show_gui_for_testing=True,
        # Parameters about furnitures
        n_round_tables=4,
        tables_positions=[[0, -3], [-3, 0], [3, 0], [0, 3]],
        table_radius=[0.5, 0.5, 0.5, 0.5],
        # Parameters about ARI
        random_start_position=1,
        random_goal_position=1,
        probability_furnitures=1,
    )
    # init of env
    env = MpiEnv(seed=seed, config=config_env)

    config_agent = config.agent

    weights = eu.AttrDict(
        navigation_encoder="./checkpoints/final_nav_encoder_state_dict.pt",
        actor="./checkpoints/final_actor_state_dict.pt",
    )

    agent = SAC(env=env.config, import_weight=weights, config=config_agent)
    trajectories = []
    for episode in range(5):
        # set trajectory for each episode
        trajectory_per_episode = list()

        # env reset
        observation, info = env.reset()
        done = False
        score = 0
        global_map = info["global_static_grid"]
        # add position to trajectory list
        trajectory_per_episode.append(tuple(env.start_position))

        while not done:
            action = agent.predict_action(observation)
            # Step env with action
            observation, reward, terminated, truncated, info = env.step(action)
            trajectory_per_episode.append(tuple(env.agent.position))
            # update of parameters
            score += reward
            done = terminated or truncated

            if env.flag_goal_reached:
                print("The goal has been reached!")
            if env.flag_collision:
                print("There was a collision")

        trajectory_per_episode.append(tuple(env.goal_position))
        trajectories.append(trajectory_per_episode)

        if config_run.trajectory_grid_show or config_run.save_plots:
            labels = [str(i) for i in range(len(trajectories))]
            plot_multiple_policies_grid(
                env, global_map, trajectories, labels, config_run
            )
    env.close()


if __name__ == "__main__":
    policy_eval(
        config_run=eu.AttrDict(trajectory_grid_show=True, n_episode=1, save_plots=False)
    )
