#!/usr/bin/env python
#  type: ignore
from hands_on_rl_navigation.exp.evaluation import Evaluation
from hands_on_rl_navigation.exp.training import run_training
from hands_on_rl_navigation.env.mpi_env.mpi_env import eval_function, log_function
import torch.nn as nn
import numpy as np
import exputils as eu
from hands_on_rl_navigation.agent.sac.sac import SAC
from hands_on_rl_navigation.env.mpi_env.mpi_env import MpiEnv


def get_reward(
    config: eu.AttrDict,
    current_position: np.array,
    former_position: np.array,
    goal_position: np.array,
    distance_to_closest_object: float,
    flag_goal_reached: bool,
    flag_collision: bool,
):
    r"""Returns the reward depending on some features:

    Inputs :
            - config (eu.AttrDict) : Dictionary with experiment configuration

            - current_position (np.array) : np.array([x,y])
                Current position of the robot

            - former_position (np.array) : np.array([x,y])
                Former position of the robot

            - goal_position (np.array) : np.array([x,y])
                Goal position of the robot

            - distance_to_closest_object (float) :
                Distance to the closest object

            - flag_goal_reached (bool) :
                Boolean that indicates if the goal is reached

            - flag_collision (bool) :
                Boolean that indicates if there is a collision


    Outputs :
        - reward_signal (float)
            Float representing the sum of all the sub-rewards
    """

    reward_signal = 0.0

    # reward_signal obtained when the episode ends (sparse reward_signal)
    if flag_goal_reached:
        reward_signal += 500.0
    elif flag_collision:
        reward_signal += -500.0
    else:
        # Reward obtained along the episode (dense reward_signal)
        pass
        # np.linalg.norm(p1-p2) : Computes the distance between positions p1 and p2

    return reward_signal


def agent_actor_network(input_shape):
    """Definition of the DRL Agent Actor network."""

    return nn.Sequential(
        nn.Linear(
            input_shape, 512
        ),  # First layer must have a dimension equal to the input_shape argument.
        nn.ReLU(),
    )


def agent_critic_network(input_shape, num_actions):
    r"""Definition of the DRL Agent Critic network.
    Note : The last layer must have a single neuron"""

    return nn.Sequential(
        nn.Linear(input_shape + num_actions, 1),
        nn.ReLU(),
    )


config = eu.AttrDict(
    seed=505,  # seed to define the random number generator
    # has to be different between repetitions of the same experiment
    n_max_episodes=300,  # number of training episodes
    n_max_episodes_eval=10,  # number of evaluation episodes
    default_exp_info=eu.AttrDict(
        frequency_saving_network=10,  # ???
        frequency_test=10,  # ???
    ),
    agent=eu.AttrDict(
        ###################################################################################
        # Hyperparameters for the RL Agent
        network_update=50,  # number of steps between each backprop
        nb_gradient_steps=20,  # number of backprop per update step
        batch_size=128,  # batch size (num of observations) of the updates
        learning_rate=0.005,  # learning rate
        discount_factor=0.99,  # discount factor of the RL agent
        policy_type="Gaussian",  # Policy Type: Gaussian | Deterministic
        ##################################################################################
        # DO NOT MODIFY
        cls=SAC,  # use SAC as DRL agent
        list_of_input_to_encode=[
            ["angle_to_goal_position", "distance_to_goal_position"],
            ["raycast"],
        ],
        list_of_input_to_memorize=[
            ["angle_to_goal_position", "distance_to_goal_position"],
            ["raycast"],
        ],
        agent_actor_network=agent_actor_network,
        agent_critic_network=agent_critic_network,
    ),
    # Environment parameters
    env=eu.AttrDict(
        # Parameters about furnitures
        probability_furnitures=0.5,  # probability of having tables per episode
        n_round_tables=4,  # number of tables in the env
        tables_positions=[
            [0, -3],
            [-3, 0],
            [3, 0],
            [0, 3],
        ],  # position of the tables [x,y]
        table_radius=[0.5, 0.5, 0.5, 0.5],  # radius of each table
        # Parameters about ARI
        random_start_position=True,  # should ARI start at a random position
        random_goal_position=True,  # should ARI's goal be at a random position
        # Used only if random_start_position and random_goal_position are False
        # start_goal_infos=eu.AttrDict(
        #     start_position = [0, 0],
        #     start_orientation = 0.,
        #     goal_position = [4, 4],
        #     goal_orientation = 0.,
        # ),
        # DO NOT MODIFY
        cls=MpiEnv,
        action_space_type="continuous",
        get_reward=get_reward,
    ),
    # config for evaluation episodes, DO NOT MODIFY
    env_eval_0=eu.AttrDict(
        probability_furnitures=1,  # do evaluation always with furnituer
    ),
    # logging functions, DO NOT MODIFY
    log_functions=[eval_function, log_function],
)


if __name__ == "__main__":
    # create the evaluation environment
    log_state = eu.AttrDict()
    log_state.evaluation_env0 = Evaluation(
        config=config, eval_id=0, config_eval=config.env_eval_0
    )

    run_training(config=config, log_state=log_state)
