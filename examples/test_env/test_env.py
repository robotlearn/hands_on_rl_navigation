#!/usr/bin/env python
#  type: ignore

# Load packages
from hands_on_rl_navigation.env.mpi_env.mpi_env import MpiEnv
import exputils as eu
import numpy as np


def test_environment():
    config = eu.AttrDict(
        show_gui_for_testing=True,  # display the 2d environment
        n_max_steps_per_epsiode=10,  # maximum number of step per episode
        n_round_tables=4,  # Number of table in the simulation
        tables_positions=[
            [0, -3],
            [3, 0],
            [-3, 0],
            [0, 3],
        ],  # # Position of tables in the simulation
        probability_furnitures=1,  # probability to have furniture in the episode
        eta_rd_furniture=0,  # Probability of having random positionned furniture (1 : random furniture | 0 : fixed furniture)
        raycast_field_of_view=360,  # Field of view of raycast
        raycast_angle_between_two_ray=10,  # Angle between two rays
        raycast_max_range=3,  # Maximum depth of rays
        draw_raycast=False,  # Display the raycast ray's
    )

    env = MpiEnv(config=config, seed=40)

    for _ in range(100):
        env.reset()
        done = False
        while not done:
            action = (
                np.random.uniform(env.vmin, env.vmax),
                np.random.uniform(env.wmin, env.wmax),
            )
            _, _, terminated, truncated, info = env.step(action)
            done = terminated or truncated


if __name__ == "__main__":
    test_environment()
