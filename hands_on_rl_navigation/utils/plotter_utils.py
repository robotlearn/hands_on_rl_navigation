#!/usr/bin/env python
#  type: ignore

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from matplotlib.cm import get_cmap
from skimage.transform import resize
import mpi_sim
from matplotlib.collections import LineCollection
from matplotlib import colors as mcolors
import matplotlib.style as style
import torch
from PIL import Image
import exputils as eu
from hands_on_rl_navigation.networks import *

# import torchvision
# import  torchvision.transforms as transforms
style.use("seaborn-colorblind")


def plot_single_policy_grid(
    env, global_map, trajectory, config_run=None, title="Trajectory grid"
):
    r""" "Plot a trajectory for a single policy".

    Important :
        > config_run is only used for trajectory_grid_show and save_plots

    """

    fig = plt.figure(1)
    fig.set_size_inches(6, 6)
    cmap2 = get_cmap("binary")
    cmap2.set_under("w")
    ax = plt.axes()

    ax.set_yticks([])
    ax.set_xticks([])
    fig.add_axes(ax)
    ax.imshow(np.flip(global_map.T, axis=0), cmap=cmap2, vmin=0, vmax=1)

    # Plot the starting point
    r, c = np.shape(global_map)
    start = mpi_sim.utils.transform_position_to_map_coordinate(
        env.start_position, env.area, env.resolution
    )
    ax.scatter(start[0], c - start[1], color="purple", label="start position")

    # Plot the goal point
    goal = mpi_sim.utils.transform_position_to_map_coordinate(
        env.goal_position, env.area, env.resolution
    )
    ax.scatter(goal[0], c - goal[1], color="red", label="goal position")

    traj_in_coord = list()
    for position in trajectory:
        new_position = mpi_sim.utils.transform_position_to_map_coordinate(
            position, env.area, env.resolution
        )
        traj_in_coord.append((new_position[0], c - new_position[1]))

    traj_in_coord = np.array(traj_in_coord).reshape(-1, 1, 2)

    segments = np.concatenate(
        (
            traj_in_coord[:-1],  # (x_i, y_i)
            traj_in_coord[1:],  # (x_i+1, y_i+1)
        ),
        axis=1,
    )

    line_collection = LineCollection(
        segments=segments,
        cmap=get_cmap("rainbow"),
        linewidths=5,
        norm=mcolors.Normalize(vmin=0, vmax=len(trajectory)),
    )

    color = np.linspace(start=0, stop=len(trajectory), num=len(trajectory))
    line_collection.set_array(color)
    ax.add_collection(line_collection)
    fig.colorbar(line_collection, label="Steps")
    ax.legend(
        loc="upper right", bbox_to_anchor=(1, 0), fancybox=True, shadow=True, ncol=2
    )
    ax.set_title(title)
    if config_run.save_plots:
        fig.savefig(config_run.path_where_to_save)
    if config_run.trajectory_grid_show:
        plt.show()


def plot_multiple_policies_with_same_length(
    env, global_map, trajectories, config_run=None, title="Trajectory grid"
):
    r""" "Plot a trajectory for a single policy".

    Important :
        > config_run is only used for trajectory_grid_show and save_plots

    """

    fig = plt.figure(1)
    fig.set_size_inches(6, 6)
    cmap2 = get_cmap("binary")
    cmap2.set_under("w")
    ax = plt.axes()

    ax.set_yticks([])
    ax.set_xticks([])
    fig.add_axes(ax)
    ax.imshow(np.flip(global_map.T, axis=0), cmap=cmap2, vmin=0, vmax=1)

    # Plot the starting point
    r, c = np.shape(global_map)
    start = mpi_sim.utils.transform_position_to_map_coordinate(
        env.start_position, env.area, env.resolution
    )
    ax.scatter(start[0], c - start[1], color="purple", label="ARI start position")

    # Plot the goal point
    goal = mpi_sim.utils.transform_position_to_map_coordinate(
        env.goal_position, env.area, env.resolution
    )
    ax.scatter(goal[0], c - goal[1], color="red", label="ARI goal position")
    for i in range(len(trajectories)):
        trajectory = trajectories[i]
        traj_in_coord = list()
        for position in trajectory:
            new_position = mpi_sim.utils.transform_position_to_map_coordinate(
                position, env.area, env.resolution
            )
            traj_in_coord.append((new_position[0], c - new_position[1]))

        traj_in_coord = np.array(traj_in_coord).reshape(-1, 1, 2)

        segments = np.concatenate(
            (
                traj_in_coord[:-1],  # (x_i, y_i)
                traj_in_coord[1:],  # (x_i+1, y_i+1)
            ),
            axis=1,
        )

        line_collection = LineCollection(
            segments=segments,
            cmap=get_cmap("rainbow"),
            linewidths=5,
            norm=mcolors.Normalize(vmin=0, vmax=len(trajectory)),
        )

        color = np.linspace(start=0, stop=len(trajectory), num=len(trajectory))
        line_collection.set_array(color)
        ax.add_collection(line_collection)
    fig.colorbar(line_collection, label="Steps")
    ax.legend(
        loc="upper right", bbox_to_anchor=(1, 0), fancybox=True, shadow=True, ncol=2
    )
    ax.set_title(title)
    if config_run.save_plots:
        fig.savefig(config_run.path_where_to_save)
    if config_run.trajectory_grid_show:
        plt.show()


def plot_multiple_policies_grid_with_same_start_ang_goal(
    env, global_map, trajectories, labels, config_run=None
):
    r""" "Plot multiple trajectories".

    Important :
        > config_run is only used for trajectory_grid_show and save_plots
        > The policies have to begin from the same point

    """

    fig = plt.figure(1)
    ax = plt.axes()
    fig.set_size_inches(6, 6)
    cmap2 = get_cmap("binary")
    cmap2.set_under("w")

    ax.set_yticks([])
    ax.set_xticks([])
    fig.add_axes(ax)
    ax.imshow(np.flip(global_map.T, axis=0), cmap=cmap2, vmin=0, vmax=1)

    # Plot the starting point
    r, c = np.shape(global_map)
    start = mpi_sim.utils.transform_position_to_map_coordinate(
        env.start_position, env.area, env.resolution
    )
    ax.scatter(start[0], c - start[1], color="purple", label="start position")

    # Plot the goal point
    goal = mpi_sim.utils.transform_position_to_map_coordinate(
        env.goal_position, env.area, env.resolution
    )
    ax.scatter(goal[0], c - goal[1], color="red", label="goal position")

    # colors = {
    #     'blue':   [55,  126, 184],  #377eb8
    #     'orange': [255, 127, 0],    #ff7f00
    #     'green':  [77,  175, 74],   #4daf4a
    #     'pink':   [247, 129, 191],  #f781bf
    #     'brown':  [166, 86,  40],   #a65628
    #     'purple': [152, 78,  163],  #984ea3
    #     'gray':   [153, 153, 153],  #999999
    #     'red':    [228, 26,  28],   #e41a1c
    #     'yellow': [222, 222, 0]     #dede00
    # }

    colors = [
        "#377eb8",
        "#ff7f00",
        "#4daf4a",
        "#f781bf",
        "#a65628",
        "#984ea3",
        "#999999",
        "#e41a1c",
        "#dede00",
    ]

    for i in range(len(trajectories)):
        trajectory = trajectories[i]
        segment = np.empty((len(trajectory), 2))
        for j in range(len(trajectory)):
            position = trajectory[j]
            new_position = mpi_sim.utils.transform_position_to_map_coordinate(
                position, env.area, env.resolution
            )
            segment[j, 0] = new_position[0]
            segment[j, 1] = c - new_position[1]
        segment = segment.reshape(1, len(trajectory), 2)
        line_collection = LineCollection(
            segments=segment,
            linewidths=5,
            colors=colors[i],
            linestyle="solid",
            label=labels[i],
        )
        ax.add_collection(line_collection)

    # ax.legend(loc='right',bbox_to_anchor=(1.5, 0.5),fancybox=True, shadow=True, ncol=1)
    ax.legend(loc=4, bbox_to_anchor=(1, 0), fancybox=True, shadow=True, ncol=3)
    # fig.savefig('test.png')
    # fig.show()

    if config_run.save_plots:
        fig.savefig(config_run.path_where_to_save)
    if config_run.trajectory_grid_show:
        plt.show()


def plot_multiple_policies_grid(env, global_map, trajectories, labels, config_run=None):
    r""" "Plot multiple trajectories".

    Important :
        > config_run is only used for trajectory_grid_show and save_plots
        > The policies have to begin from the same point

    """

    fig = plt.figure(1)
    ax = plt.axes()
    fig.set_size_inches(6, 6)
    cmap2 = get_cmap("binary")
    cmap2.set_under("w")

    ax.set_yticks([])
    ax.set_xticks([])
    fig.add_axes(ax)
    ax.imshow(np.flip(global_map.T, axis=0), cmap=cmap2, vmin=0, vmax=1)

    # Plot the starting point
    r, c = np.shape(global_map)

    # colors = {
    #     'blue':   [55,  126, 184],  #377eb8
    #     'orange': [255, 127, 0],    #ff7f00
    #     'green':  [77,  175, 74],   #4daf4a
    #     'pink':   [247, 129, 191],  #f781bf
    #     'brown':  [166, 86,  40],   #a65628
    #     'purple': [152, 78,  163],  #984ea3
    #     'gray':   [153, 153, 153],  #999999
    #     'red':    [228, 26,  28],   #e41a1c
    #     'yellow': [222, 222, 0]     #dede00
    # }

    colors = [
        "#E69F00",
        "#CC79A7",
        "#009E73",
        "#F0E442",
        "#56B4E9",
        "#0072B2",
        "#F0E442",
        "#56B4E9",
        "#000000",
    ]

    for i in range(len(trajectories)):
        trajectory = trajectories[i]

        start = mpi_sim.utils.transform_position_to_map_coordinate(
            trajectory[0], env.area, env.resolution
        )
        ax.scatter(start[0], c - start[1], color="purple")

        # Plot the goal point
        goal = mpi_sim.utils.transform_position_to_map_coordinate(
            trajectory[-1], env.area, env.resolution
        )
        ax.scatter(goal[0], c - goal[1], color="red")
        # trajectory.pop(-1)
        segment = np.empty((len(trajectory) - 1, 2))
        for j in range(len(trajectory) - 1):
            position = trajectory[j]
            new_position = mpi_sim.utils.transform_position_to_map_coordinate(
                position, env.area, env.resolution
            )
            segment[j, 0] = new_position[0]
            segment[j, 1] = c - new_position[1]
        segment = segment.reshape(1, len(trajectory) - 1, 2)
        line_collection = LineCollection(
            segments=segment,
            linewidths=5,
            colors=colors[i],
            linestyle="solid",
            label=labels[i],
        )
        ax.add_collection(line_collection)

    # ax.legend(loc='right',bbox_to_anchor=(1.5, 0.5),fancybox=True, shadow=True, ncol=1)
    ax.legend(
        loc="upper right", bbox_to_anchor=(0.8, 0), fancybox=True, shadow=True, ncol=3
    )
    # fig.savefig('test.png')
    # fig.show()

    if config_run.save_plots:
        fig.savefig(config_run.path_where_to_save)
    if config_run.trajectory_grid_show:
        plt.show()
