#!/usr/bin/env python
#  type: ignore

import warnings
import exputils as eu
import numpy as np
from hands_on_rl_navigation.env.mpi_env.utils.furnitures import *
from hands_on_rl_navigation.env.mpi_env.utils.other import *


def set_start_position(objects_in_simulation, self):
    r"""Set start position of robot"""
    if self.config.random_start_position:
        # sample start pos
        [[x_min, x_max], [y_min, y_max]] = self.ari_start_region
        start_position_ok = False
        count = 0
        while not start_position_ok and count < 50:
            count += 1

            start_position = [
                round(np.random.uniform(x_min, x_max), 1),
                round(np.random.uniform(y_min, y_max), 1),
            ]

            # if self.config.human_scenario == 'circle':
            #     start_position[0] = round(np.random.uniform(-start_position[1], start_position[1]), 1)

            start_position_ok = distance_between_objects_is_okay(
                self=self,
                p=start_position,
                list_object=objects_in_simulation,
                minimum_distance=self.minimum_distance_between_position_and_object,
            )
            if not start_position_ok and count == 50:
                warnings.warn("problem with start position {}".format(start_position))
        # Overwriting with the new random position

        self.start_position = start_position
        self.start_orientation = round(np.random.uniform(-np.pi, np.pi), 1)

    elif "moving_start_pos" in self.config:
        self.start_position = self.config.moving_start_pos[self.nb_reset]


def set_start_and_goal_positions(self):
    r"""Set start and goal position of robot"""
    objects_in_simulation = self.simulation.get_objects_in_simulation(
        object_to_avoid=self.agent
    )

    set_start_position(objects_in_simulation=objects_in_simulation, self=self)

    if self.config.random_goal_position:
        # sample goal pos
        [[x_min, x_max], [y_min, y_max]] = self.ari_goal_region
        goal_position_ok = False
        count = 0
        while not goal_position_ok and count < 500:
            count += 1
            goal_position = [
                round(np.random.uniform(x_min, x_max), 1),
                round(np.random.uniform(y_min, y_max), 1),
            ]
            # if self.config.human_scenario == 'circle':
            #     goal_position = -np.array(self.start_position)

            goal_position_away_from_obstacle = distance_between_objects_is_okay(
                self=self,
                p=goal_position,
                list_object=objects_in_simulation,
                minimum_distance=self.minimum_distance_between_position_and_object,
            )
            goal_position_away_from_start_position = check_distance_between_positions(
                position=goal_position,
                position_to_avoid=self.start_position,
                minimum_distance=self.minimum_distance_between_start_goal,
            )
            goal_position_ok = (
                goal_position_away_from_obstacle
                and goal_position_away_from_start_position
            )
            if not goal_position_ok and count % 50 == 0:
                set_start_position(
                    objects_in_simulation=objects_in_simulation, self=self
                )
            if not goal_position_ok and count == 500:
                warnings.warn("problem with goal position {}".format(goal_position))

        # Overwriting with the new random position
        self.goal_position = goal_position

    elif "moving_goal_pos" in self.config:
        self.goal_position = self.config.moving_goal_pos[self.nb_reset]


def ARI_leaving(pos, boundaries=5):
    r"""Check if the ARI robot is leaving the room"""
    x_ARI, y_ARI = pos
    # if (x_ARI - boundaries) * (- boundaries - x_ARI) < 0 or (y_ARI - boundaries) * (- boundaries - y_ARI) < 0 :
    if (x_ARI > boundaries or x_ARI < -boundaries) or (
        y_ARI > boundaries or y_ARI < -boundaries
    ):
        print("ERROR : Bug on the simulator, ARI is leaving the scene")
        return True
    else:
        return False
