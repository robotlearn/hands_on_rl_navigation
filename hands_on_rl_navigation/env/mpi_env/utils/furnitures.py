#!/usr/bin/env python
#  type: ignore

import numpy as np
import mpi_sim
from copy import deepcopy


def set_furniture_with_random_position(self):
    """Add furniture randomly with the probability p = 1 - eta_rd_furniture
    We don't want ARI and itsgoal on an object
    """

    simulation = self.simulation
    furniture_apparition = self.flag_furniture_apparition
    n_round_tables = self.n_round_tables
    radius = self.config.radius
    n_chairs = self.n_chairs
    round_tables_region = self.config.round_tables_region
    chairs_region = self.config.chairs_region
    start_position = self.start_position
    goal_position = self.goal_position

    # all_objects_in_sim = deepcopy(self.furnitures_position_in_simulation)
    all_objects_in_sim = deepcopy(
        self.simulation.get_objects_in_simulation(object_to_avoid=self.agent)
    )
    all_objects_in_sim.append(start_position)
    all_objects_in_sim.append(goal_position)

    if furniture_apparition:
        # add tables with random positions
        for tables_idx in range(n_round_tables):
            (
                x_pos_round_tables,
                y_pos_round_tables,
                orientation,
            ) = generate_compatible_position_for_object(
                idx=tables_idx,
                objects_in_sim=all_objects_in_sim,
                object_region=round_tables_region,
                nb_furniture=n_round_tables,
                grid_mode=0,
                grid_mode_resolution=0,
                minimum_distance=self.minimum_distance_between_object_and_object,
            )

            # add table to simulation
            round_table = mpi_sim.objects.RoundTable(
                position=[x_pos_round_tables, y_pos_round_tables],
                orientation=orientation,
                radius=radius,
            )
            simulation.add_object(round_table)
            all_objects_in_sim.append([x_pos_round_tables, y_pos_round_tables])
            # self.furnitures_position_in_simulation.append([x_pos_round_tables, y_pos_round_tables])

        ### Create furnitures - Chair
        # add chairs with random positions
        for chairs_idx in range(n_chairs):
            # sample start pos

            (
                x_pos_chairs,
                y_pos_chairs,
                orientation,
            ) = generate_compatible_position_for_object(
                idx=chairs_idx,
                objects_in_sim=all_objects_in_sim,
                object_region=chairs_region,
                nb_furniture=n_chairs,
                grid_mode=0,
                grid_mode_resolution=0,
                minimum_distance=self.minimum_distance_between_object_and_object,
            )

            # add chairs to simulation
            chair = mpi_sim.objects.Chair(
                position=[x_pos_chairs, y_pos_chairs], orientation=orientation
            )
            simulation.add_object(chair)
            all_objects_in_sim.append([x_pos_chairs, y_pos_chairs])
            # self.furnitures_position_in_simulation.append([x_pos_chairs, y_pos_chairs])
        # raise Warning('Check that the position are effectively added to the simulation')


def set_furniture_with_fixed_position_given_type(
    self,
    type: str,
    num: int,
    pos: list,
    simulation,
    radius=0.5,
    orientation=0,
    width=0.2,
    length=0.5,
):
    r"""Set furniture on a fixed position with given tyoe"""
    # sample start pos
    if isinstance(radius, list):
        radius_list = deepcopy(radius)
    else:
        radius_list = None
    if isinstance(length, list):
        length_list = deepcopy(length)
    else:
        length_list = None
    if isinstance(width, list):
        width_list = deepcopy(width)
    else:
        width_list = None
    if isinstance(orientation, list):
        orientation_list = deepcopy(orientation)
    else:
        orientation_list = None

    if type == "RoundTable":
        for idx in range(num):
            # add table to simulation
            x_pos = pos[idx][0]
            y_pos = pos[idx][1]

            if isinstance(radius_list, list):
                radius = radius_list[idx]
            if isinstance(orientation_list, list):
                orientation = orientation_list[idx]

            round_table = mpi_sim.objects.RoundTable(
                position=[x_pos, y_pos], orientation=orientation, radius=radius
            )
            simulation.add_object(round_table)
    # self.furnitures_position_in_simulation.append([x_pos, y_pos])
    elif type == "Chair":
        for idx in range(num):
            x_pos = pos[idx][0]
            y_pos = pos[idx][1]

            if isinstance(orientation_list, list):
                orientation = orientation_list[idx]

            chair = mpi_sim.objects.Chair(
                position=[x_pos, y_pos], orientation=orientation
            )
            simulation.add_object(chair)
            # self.furnitures_position_in_simulation.append([x_pos, y_pos])
    else:
        raise NotImplementedError


def compute_nearest_furniture_with_simulation_measurement(self):
    """Compute the distance to furniture in the simulation only with nearby furnitures"""
    min_distance = np.sqrt(
        (-self.xmin + self.xmax) ** 2 + (-self.ymin + self.ymax) ** 2
    )
    angle_to_nearest_object = 0
    nearest_obj = None
    for obj in self.nearby_objects:
        distance_to_obj = np.round(
            mpi_sim.utils.measure_distance(self.agent, obj).distance, 3
        )
        if distance_to_obj < min_distance:
            nearest_obj = obj
            min_distance = distance_to_obj
            angle_to_nearest_object = np.round(
                mpi_sim.utils.measure_relative_angle(self.agent, nearest_obj), 3
            )
    # print(np.degrees(angle_to_nearest_object), min_distance)
    # Noise data if necessary
    angle_to_nearest_object += self.config.noise_nearest_object * (
        np.random.normal(0, 1, size=np.shape(angle_to_nearest_object))
    )
    min_distance += self.config.noise_nearest_object * (
        np.random.normal(0, 1, size=np.shape(min_distance))
    )

    # Note : can also return the nearest_obj if needed
    return angle_to_nearest_object, min_distance


def set_furniture_with_fixed_position(self):
    """Set furniture with fixed position. Probability of apparition equal to p = \'probability_furnitures\'"""

    if len(self.config.table_radius) == 0:
        table_radius = []
        for _ in range(self.n_round_tables):
            table_radius.append(np.random.choice([0.05, 0.1, 0.2, 0.3, 0.4]))
    else:
        table_radius = self.config.table_radius

    if self.flag_furniture_apparition:
        ### Create round table with fixed position

        set_furniture_with_fixed_position_given_type(
            self=self,
            type="RoundTable",
            num=self.n_round_tables,
            pos=self.config.tables_positions,
            radius=table_radius,
            simulation=self.simulation,
        )

        set_furniture_with_fixed_position_given_type(
            self=self,
            type="Chair",
            num=self.n_chairs,
            pos=self.config.chairs_positions,
            radius=0,
            simulation=self.simulation,
            orientation=self.config.chairs_orientations,
        )


def generate_compatible_position_for_object(
    idx: int,
    objects_in_sim: list,
    object_region: list,
    nb_furniture: int,
    grid_mode: bool,
    grid_mode_resolution: float,
    minimum_distance=2,
):
    r"""Generates a position for given object so that it does not overlap"""
    min_start_x, max_start_x = object_region[0]
    min_start_y, max_start_y = object_region[1]
    object_ok = False
    count = 0

    # sample start pos
    while not object_ok and count < 500:
        rand_nums = np.random.rand(nb_furniture, 3)
        count += 1
        x_pos = min_start_x + rand_nums[idx][0] * (max_start_x - min_start_x)
        y_pos = min_start_y + rand_nums[idx][1] * (max_start_y - min_start_y)

        object_ok = distance_between_objects_is_okay(
            [x_pos, y_pos],
            list_object=objects_in_sim,
            minimum_distance=minimum_distance,
        )

        # object_ok = object_overlapping_with_other_object

        if count >= 500:
            # No solution found so we put the object outside of the simulation
            x_pos = 8
            y_pos = 8

    orientation = -np.pi + rand_nums[idx][2] * 2 * np.pi
    return (x_pos, y_pos, orientation)


def distance_between_objects_is_okay(p, list_object, self=None, minimum_distance=0.5):
    """Return True is the distance between a position (or Object) and a list of objects in above a given threshold"""
    if isinstance(p, mpi_sim.Object):
        # Distance between two given objects
        for obj_pos in list_object:
            if mpi_sim.utils.measure_distance(p, obj_pos).distance <= minimum_distance:
                return False
    else:
        if self is None:
            # Distance between a given position and each center of the objects of the simulation
            for obj_pos in list_object:
                try:
                    pos = obj_pos.position
                except:
                    pos = obj_pos
                if mpi_sim.utils.measure_distance(p, pos).distance <= minimum_distance:
                    return False
        else:
            # raise NotImplementedError('The measurement of the distance between a given position and each objects of the simulation is not implemented yet!')
            # Distance between a given position and each objects of the simulation
            for obj_pos in list_object:
                if (
                    mpi_sim.utils.measure_distance(p, obj_pos, self.simulation).distance
                    <= minimum_distance
                ):
                    return False

    return True
