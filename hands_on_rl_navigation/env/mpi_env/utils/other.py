#!/usr/bin/env python
#  type: ignore

import warnings
import exputils as eu
import numpy as np
import mpi_sim
from copy import deepcopy


def get_reward(
    config: eu.AttrDict,
    current_position: np.array,
    former_position: np.array,
    goal_position: np.array,
    distance_to_closest_object: float,
    flag_goal_reached: bool,
    flag_collision: bool,
):
    r"""Compute the reward depending on the config weight

    Inputs :
            - config (eu.AttrDict) :
                Dictionary that contains the following attributes
                * reward_goal_reached
                * reward_collision
                * reward_per_step
                * reward_warning_zone
                * reward_distance_progress_weight

            - current_position (np.array) : np.array([x,y])
                Current position of the robot

            - former_position (np.array) : np.array([x,y])
                Former position of the robot

            - goal_position (np.array) : np.array([x,y])
                Goal position of the robot

            - distance_to_closest_object (float) :
                Distance to the closest object

            - flag_goal_reached (bool) :
                Boolean that indicates if the goal is reached

            - flag_collision (bool) :
                Boolean that indicates if there is a collision


    Outputs :
        - reward_dict (eu.AttrDict) :
            Dictionary that contains the following attributes
                * reward_goal_reached
                * reward_collision
                * reward_per_step
                * reward_warning_zone
                * reward_distance_progress
            Corresponding to each reward value

        - reward_signal (float)
            Float representing the sum of all the sub-rewards

    """
    reward_dict = eu.AttrDict()
    reward_dict.reward_goal_reached = 0
    reward_dict.reward_collision = 0
    reward_dict.reward_distance_progress = 0
    reward_dict.reward_per_step = 0
    reward_dict.reward_warning_zone = 0
    reward_signal = 0

    # reward_signal obtained when the episode ends (sparse reward_signal)
    if flag_goal_reached:
        reward_dict.reward_goal_reached = config.reward_goal_reached
        reward_signal += config.reward_goal_reached
        reward_dict.reward_collision = 0

    elif flag_collision:
        reward_dict.reward_collision = config.reward_collision
        reward_signal += config.reward_collision
        reward_dict.reward_goal_reached = 0

    else:
        # Reward obtained along the episode (dense reward_signal)
        if config.reward_distance_progress_weight:
            current_distance_to_goal = np.linalg.norm(current_position - goal_position,)
            former_distance_to_goal = np.linalg.norm(former_position - goal_position)
            distance_progress = former_distance_to_goal - current_distance_to_goal
            reward_distance_progress = (
                config.reward_distance_progress_weight * distance_progress
            )
            reward_dict.reward_distance_progress = reward_distance_progress
            reward_signal += reward_distance_progress

        if config.reward_per_step:
            reward_dict.reward_per_step = config.reward_per_step
            reward_signal += config.reward_per_step

        if distance_to_closest_object <= config.distance_threshold_safety:
            reward_dict.reward_warning_zone = config.reward_warning_zone
            reward_signal += config.reward_warning_zone

    return reward_signal


def set_action_list(
    linear_velocties_list=[0, 0.2, 0.4, 0.6],
    angular_velocities_list=[
        -np.pi / 4,
        -np.pi / 6,
        -np.pi / 12,
        0,
        np.pi / 12,
        np.pi / 6,
        np.pi / 4,
    ],
) -> list:
    r"""Function that returns a list of discrete pairs of actions

    Args :
        linear_velocties_list (list)
            default value : [0, 0.2, 0.4, 0.6]

        angular_velocities_list (list)
            default value :  [-    config.log_functions = [log_function]

    # create the evaluation env
    log_state.evaluation_env0 = Evaluation(confignp.pi/4, -np.pi/6, -np.pi/12, 0, np.pi/12, np.pi/6, np.pi/4]

    Return :
        actions (list)

    """
    actions = []
    for linear_velocity in linear_velocties_list:
        for angular_velocity in angular_velocities_list:
            # if linear_velocity == 0 and angular_velocity == 0:
            #     pass
            # else :

            # a single step of simulation is making the robot moving forward of 10 cm
            # So with 5 loop of simulation the robot is moving forward with 0.5m
            actions.append([linear_velocity, round(angular_velocity, 1)])

    return actions


def check_distance_between_positions(position, position_to_avoid, minimum_distance):
    r"""Check if the distance between two position is inferior to a given minimum threshold"""
    if (
        mpi_sim.utils.measure_center_distance(position, position_to_avoid)
        <= minimum_distance
    ):
        return False
    else:
        return True
