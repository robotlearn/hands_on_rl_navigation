#!/usr/bin/env python
#  type: ignore

import exputils as eu
import numpy as np
import mpi_sim
import torch
import os
from hands_on_rl_navigation.env.mpi_env.utils.furnitures import *
from hands_on_rl_navigation.env.mpi_env.utils.robot import *
from hands_on_rl_navigation.env.mpi_env.utils.other import *
from hands_on_rl_navigation.agent import *
import warnings
import gymnasium as gym


class MpiEnv(gym.Env):
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # Episode parameter
            show_gui_for_testing=False,
            n_max_steps_per_epsiode=300,
            # Parameters about ARI
            ari_start_region=[[-4, 4], [-4, 4]],  # [[x_min, x_max], [y_min, y_max]]
            ari_goal_region=[[-4, 4], [-4, 4]],  # [[x_min, x_max], [y_min, y_max]]
            random_start_position=0,
            random_goal_position=0,
            minimum_distance_between_start_goal=1,  # The agent starts and goal are 1m far from every object
            minimum_distance_between_position_and_object=1,
            minimum_distance_between_object_and_object=2,
            start_goal_infos=eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                start_position=[0, 0],
                start_orientation=0,
                goal_position=[4, 4],
                goal_orientation=0.0,
            ),
            action_space_type="discrete",
            nb_actions=28,
            vmin=0,
            vmax=0.5,
            wmin=-0.5,
            wmax=0.5,
            list_of_input=[
                ["raycast"],
                ["angle_to_goal_position", "distance_to_goal_position"],
            ],
            # Parameter for nearest object detection
            noise_nearest_object=0,
            # Parameter for Raycast Component
            compute_raycast_with_map=False,
            compute_distance_from_obstacle_with_map=False,
            raycast_field_of_view=360,
            raycast_angle_between_two_ray=10,
            raycast_max_range=3,
            draw_raycast=False,
            raycast_noise_ratio=0,
            # Parameters about furnitures
            scenes=None,
            object_between_start_and_goal=0,
            eta_rd_furniture=0,
            probability_furnitures=0.5,
            n_round_tables=0,
            n_chairs=0,
            radius=0.5,
            tables_positions=[
                [0, -3],
                [4, 4],
                [-3, 0],
                [4, -4],
                [-4, -4],
                [3, 0],
                [-4, 4],
                [0, 3],
                [0, 0],
            ],
            table_radius=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
            chairs_positions=[[2, 0], [-2, 0], [0, 0], [0, -1.5]],
            chairs_orientations=[0.0, -np.pi / 2, np.pi / 2, np.pi],
            # Furnitures region if randomized furnitures
            round_tables_region=[[-3, 3], [-3, 3]],  # [[x_min, x_max], [y_min, y_max]]
            chairs_region=[[-3, 3], [-3, 3]],  # [[x_min, x_max], [y_min, y_max]]
            # Common parameters
            distance_threshold_goal_reached=0.1,
            distance_threshold_collision=0.5,
            distance_threshold_safety=2,
            # Reward parameters
            reward_distance_progress_weight=10,
            reward_warning_zone=0,
            reward_per_step=-5,
            reward_goal_reached=500,
            reward_collision=-500,
        )

    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.n_max_steps_per_epsiode = self.config.n_max_steps_per_epsiode
        self.cur_step = 0
        self.unsorted_list_of_input = []
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        for elt in self.config.list_of_input:
            if isinstance(elt, str):
                self.unsorted_list_of_input.append(elt)
            else:
                self.unsorted_list_of_input.extend(elt)

        self.entities_positions_global = None
        self.entities_relative_orientation = None
        self.entities_velocity = None
        self.entities_trajectory = None

        processes_config = []
        if self.config.show_gui_for_testing:
            processes_config.append({"type": "GUI"})

        # Limit of the room
        self.xmin = -5
        self.xmax = 5
        self.ymin = -5
        self.ymax = 5
        # Area to show in the global map
        self.config.ground_boundaries_in_coords = [
            [self.xmin, self.xmax],
            [self.ymin, self.ymax],
        ]
        self.area = ((self.xmin - 1, self.xmax + 1), (self.ymin - 1, self.ymax + 1))
        self.area_in_room = (
            (self.xmin + 1, self.xmax - 1),
            (self.ymin + 1, self.ymax - 1),
        )

        # Simulation creation

        # Definition of start and goal position/orientation
        self.start_position = self.config.start_goal_infos.start_position
        self.start_orientation = self.config.start_goal_infos.start_orientation
        self.goal_position = self.config.start_goal_infos.goal_position
        self.goal_orientation = self.config.start_goal_infos.goal_orientation
        self.ari_start_region = self.config.ari_start_region
        self.ari_goal_region = self.config.ari_goal_region

        # Place holder for the furnitures
        self.n_round_tables = self.config.n_round_tables
        self.n_chairs = self.config.n_chairs

        # Randomness of the furniture
        # ex : eta_rd_furniture = 0 means that the furnitures have a fixed position for each episode
        #       eta_rd_furniture = 1 means that the furniture have a random position for each episode
        self.eta_rd_furniture = self.config.eta_rd_furniture
        self.flag_random_furniture = None
        self.flag_furniture_apparition = None

        self.env_with_furnitures = bool(self.n_round_tables)

        self.furnitures_position_in_simulation = list()
        self.minimum_distance_between_position_and_object = (
            self.config.minimum_distance_between_position_and_object
        )
        self.minimum_distance_between_object_and_object = (
            self.config.minimum_distance_between_object_and_object
        )
        self.minimum_distance_between_start_goal = (
            self.config.minimum_distance_between_start_goal
        )

        self.simulation = mpi_sim.Simulation(
            visible_area=self.area,
            max_human_nb=1,  # +1 is added only for the simulation object
            objects=[
                {
                    "type": "Wall",
                    "position": [-0.1 + self.xmin, 0],
                    "orientation": np.pi,
                    "length": 10.2,
                },
                {
                    "type": "Wall",
                    "position": [0.1 + self.xmax, 0],
                    "orientation": 0.0,
                    "length": 10.2,
                },
                {
                    "type": "Wall",
                    "position": [0.0, self.ymax + 0.1],
                    "orientation": np.pi / 2,
                    "length": 10.2,
                },
                {
                    "type": "Wall",
                    "position": [0.0, -0.1 + self.ymin],
                    "orientation": -np.pi / 2,
                    "length": 10.2,
                },
            ],
            processes=processes_config,
        )

        self.resolution = 0.05  # 1 pixel = 0.05 m
        self.raycast_field_of_view = self.config.raycast_field_of_view
        self.raycast_angle_between_two_ray = self.config.raycast_angle_between_two_ray
        self.raycast_max_range = self.config.raycast_max_range
        self.raycast_size = int(
            self.raycast_field_of_view / self.raycast_angle_between_two_ray + 1
        )

        # Definition of the agent
        self.agent = mpi_sim.objects.ARIRobot(
            components=[
                mpi_sim.AttrDict(
                    type=mpi_sim.components.RayCastSensor,
                    name="raycast",
                    field_of_view=self.raycast_field_of_view,
                    delta_angle=self.raycast_angle_between_two_ray,
                    ray_max_range=self.raycast_max_range,
                    draw_raycast=self.config.draw_raycast,
                    max_raycast_shape="square",
                    noise_ratio=self.config.raycast_noise_ratio,
                    update_raycast_every_step=self.config.draw_raycast,
                )
            ],
            position=self.start_position,
            orientation=self.start_orientation,
        )

        self.current_position = self.start_position
        self.former_position = self.start_position
        self.current_orientation = None
        self.former_orientation = None

        self.simulation.add_object(self.agent)
        self.simulation.set_reset_point()

        self.memory_observation = []
        self.memory_action = []

        # Action Space definition
        self.vmin = self.config.vmin
        self.wmin = self.config.wmin
        self.vmax = self.config.vmax
        self.wmax = self.config.wmax

        # Velocitites
        self.linear_velocity = None
        self.angular_velocity = None
        self.x_velocity = None
        self.y_velocity = None
        self.former_linear_velocity = None
        self.former_angular_velocity = None

        # Distance to goal
        self.distance_to_goal = None

        # Angle to goal
        self.angle_to_goal = None

        # Distance between goal orientation and current orientation
        self.orientation_difference_ari_goal = None
        self.former_orientation_difference_ari_goal = None

        # Min distance to entities
        # Furniture
        self.nearby_objects = None
        self.distance_to_closest_object = None
        self.former_distance_to_closest_object = None
        self.distance_to_closest_object_during_episode = list()

        # Raycast of the agent
        self.raycast_distances = None

        # Nb loop of env
        self.nb_reset = 0

        # Angular velocity mesurement
        self.former_angular_velocity = None

        # Flags
        self.flag_goal_reached = None
        self.flag_collision = None
        self.flag_truncated = None

    def close(self):
        self.nb_reset = 0

    @property
    def action_space(self):
        if self.config.action_space_type == "discrete":
            return gym.spaces.Discrete(self.config.nb_actions)

        elif self.config.action_space_type == "continuous":
            return gym.spaces.Box(
                low=np.array([self.vmin, self.wmin]),  # v_min / w_min
                high=np.array([self.vmax, self.wmax]),  # v_max / w_max
                shape=((2,)),
                dtype=np.float32,
            )

    @property
    def action_spec(self):
        return self.action_space

    @property
    def observation_space(self):
        observation = eu.AttrDict()

        for e in self.unsorted_list_of_input:
            if e == "raycast":
                observation[e] = gym.spaces.Box(
                    low=0, high=3, shape=((1, self.raycast_size)), dtype=np.float32
                )
            elif e == "distance_to_goal_position":
                observation[e] = gym.spaces.Box(
                    low=0,
                    high=np.sqrt(
                        (-self.xmin + self.xmax) ** 2 + (-self.ymin + self.ymax) ** 2
                    ),
                    shape=((1, 1)),
                    dtype=np.float32,
                )
            elif e == "angle_to_goal_position":
                observation[e] = gym.spaces.Box(
                    low=-np.pi, high=np.pi, shape=((1, 1)), dtype=np.float32
                )
            else:
                raise UserWarning("Input name : {} is not recognized".format(e))

        return gym.spaces.Dict(observation.toDict())

    @property
    def collision_flag(self):
        return self.flag_collision

    @property
    def goal_reached_flag(self):
        return self.flag_goal_reached

    @property
    def terminated_flag(self):
        return self.flag_terminated

    @property
    def truncated_flag(self):
        return self.flag_truncated

    def reset(self, seed=None, options=None):
        observation, info = None, {}
        self.distance_to_closest_object_during_episode = list()
        self.linear_velocity = 0
        self.angular_velocity = 0
        self.former_linear_velocity = self.linear_velocity
        self.former_angular_velocity = self.angular_velocity

        self.cur_step = 0

        # set a reset point before the first reset and execution
        if not self.simulation.has_reset_point:
            self.simulation.set_reset_point()
        else:
            self.simulation.reset()

        ################################################
        ########## Setting the fixed furnitres #########
        ################################################

        self.furnitures_position_in_simulation = list()
        if self.env_with_furnitures:
            self.flag_furniture_apparition = (
                np.random.random() <= self.config.probability_furnitures
            )

            if self.flag_furniture_apparition:
                if np.random.random() < self.eta_rd_furniture:
                    self.flag_random_furniture = (
                        True  # Defines if the furnitures are positionned randomly
                    )

                if (
                    not self.flag_random_furniture
                ):  # Case with fixed positionned furnitures
                    if "moving_obj_pos" not in self.config:
                        # self.config.cubes_orientations = list(np.random.uniform(-np.pi, np.pi, size = np.shape(self.config.cubes_orientations)).astype('float'))
                        # self.config.rectangles_orientations = list(np.random.uniform(-np.pi, np.pi, size = np.shape(self.config.rectangles_orientations)).astype('float'))
                        set_furniture_with_fixed_position(self)

                    else:
                        if "moving_obj_type" not in self.config:
                            moving_obj_type = "RoundTable"
                        else:
                            moving_obj_type = self.config.moving_obj_type

                        if "moving_obj_radius" not in self.config:
                            moving_obj_radius = 0.5
                        else:
                            moving_obj_radius = self.config.moving_obj_radius

                        set_furniture_with_fixed_position_given_type(
                            self=self,
                            type=moving_obj_type,
                            num=1,
                            pos=[self.config.moving_obj_pos[self.nb_reset]],
                            radius=moving_obj_radius,
                            simulation=self.simulation,
                        )

        ##########################################
        #### Setting the start/goal positions ####
        ##########################################
        if self.config.random_goal_position or self.config.random_start_position:
            if not self.flag_furniture_apparition:
                self.ari_start_region = self.config.ari_start_region
                self.ari_goal_region = self.config.ari_goal_region
            set_start_and_goal_positions(self)

        # Definition of the agent's position
        self.agent.position = self.start_position
        self.agent.orientation = self.start_orientation

        #################################################
        ########## Setting the random furnitres #########
        #################################################

        if self.env_with_furnitures and self.flag_furniture_apparition:
            if self.flag_random_furniture:
                set_furniture_with_random_position(self=self)

        # First step to initialize everything in the simulation
        self.simulation.step()

        # Once every object and entities position is defined we can create the occupancy grid
        self.distance_to_goal = mpi_sim.utils.measure_center_distance(
            self.agent, self.goal_position
        )
        self.angle_to_goal = mpi_sim.utils.measure_relative_angle(
            [self.agent.position[0], self.agent.position[1], self.agent.orientation],
            self.goal_position,
        )

        self.current_position = np.array(self.agent.position)
        self.current_orientation = self.agent.orientation
        self.former_position = self.current_position
        self.former_orientation = self.current_orientation

        self.nb_reset += 1

        self.orientation_difference_ari_goal = mpi_sim.utils.angle_difference(
            self.agent.orientation, self.goal_orientation
        )
        self.former_orientation_difference_ari_goal = (
            self.orientation_difference_ari_goal
        )

        self.nearby_objects = self.simulation.get_nearby_objects(
            point=self.agent,
            radius=5,
            object_types=[
                mpi_sim.objects.Wall,
                mpi_sim.objects.RoundTable,
                mpi_sim.objects.Chair,
                mpi_sim.objects.Bench,
            ],
        )

        (
            _,
            self.distance_to_closest_object,
        ) = compute_nearest_furniture_with_simulation_measurement(self)

        # Update the raycast and the coord of closest obstacle
        self.raycast_distances = deepcopy(self.agent.raycast.raycast_distances)

        self.global_grid_dict_static = mpi_sim.utils.create_occupancy_grid_map(
            self.simulation,
            resolution=self.resolution,
            object_filter=dict(
                properties="static"
            ),  # object_filter = dict(types = [mpi_sim.objects.Bench, mpi_sim.objects.Chair, mpi_sim.objects.RoundTable])#
        )

        observation = eu.AttrDict()

        for e in self.unsorted_list_of_input:
            if e == "raycast":
                observation.raycast = np.array(self.raycast_distances).astype(
                    dtype=np.float32
                )
            elif e == "distance_to_goal_position":
                observation.distance_to_goal_position = np.array(
                    self.distance_to_goal
                ).astype(dtype=np.float32)
            elif e == "angle_to_goal_position":
                observation.angle_to_goal_position = np.array(
                    self.angle_to_goal
                ).astype(dtype=np.float32)
            else:
                raise UserWarning("Input name : {} is not recognized".format(e))

        info = {"global_static_grid": self.global_grid_dict_static.map}
        return observation, info

    def step(self, action=None):
        self._reset_flags()
        self.cur_step += 1
        self.former_linear_velocity = self.linear_velocity
        self.former_angular_velocity = self.angular_velocity

        ##############################
        ######## Action Space ########
        ##############################

        # the number of action taken by the agent depend on the config parameter : nb_actions
        # the neural net will only have "nb_actions" output
        # So it will only make the decision between 0 and "nb_actions"
        if action is None:
            raise UserWarning("An action has to be provided in RL navigation mode!")
        else:
            if isinstance(action, (int, np.int64)):
                action = int(action)
                # discrete action space
                if self.config.nb_actions != len(self.action_list):
                    # Check of total number of action defined by the list length
                    raise ValueError(
                        "The variable nb_actions does not correspond to the real nb of action, modify the config and set it to "
                        + str(len(self.action_list))
                    )
                if action >= len(self.action_list):
                    # Check that the given action is feasible
                    raise ValueError(
                        "The inputed action does not exists, the actions can be from 0 to "
                        + str(len(self.action_list) - 1)
                    )
                linear_velocity, angular_velocity = self.action_list[action]

                self._step_simulation(
                    linear_velocity=linear_velocity, angular_velocity=angular_velocity
                )

            elif isinstance(action, (tuple, np.ndarray)):
                # continuous action space
                linear_velocity = action[0]
                angular_velocity = action[1]

                self._step_simulation(
                    linear_velocity=linear_velocity, angular_velocity=angular_velocity
                )

            else:
                raise TypeError(
                    "action has the wrong type, it can either be an int or a tuple"
                )

        # Compute the nearby objects
        self.nearby_objects = self.simulation.get_nearby_objects(
            point=self.agent,
            radius=5,
            object_types=[
                mpi_sim.objects.Wall,
                mpi_sim.objects.RoundTable,
                mpi_sim.objects.Chair,
                mpi_sim.objects.Bench,
            ],
        )

        self.current_position = self.agent.position
        self.distance_to_goal = mpi_sim.utils.measure_center_distance(
            self.agent, self.goal_position
        )
        self.orientation_difference_ari_goal = mpi_sim.utils.angle_difference(
            self.agent.orientation, self.goal_orientation
        )

        self.distance_to_closest_object_during_episode.append(
            self.distance_to_closest_object
        )
        self.angle_to_goal = mpi_sim.utils.measure_relative_angle(
            [self.agent.position[0], self.agent.position[1], self.agent.orientation],
            self.goal_position,
        )

        if "get_reward" in self.config:
            reward = self.config.get_reward(
                config=self.config,
                current_position=self.current_position,
                former_position=self.former_position,
                goal_position=self.goal_position,
                distance_to_closest_object=self.distance_to_closest_object,
                flag_goal_reached=self.flag_goal_reached,
                flag_collision=self.flag_collision,
            )
        else:
            reward = get_reward(
                config=self.config,
                current_position=self.current_position,
                former_position=self.former_position,
                goal_position=self.goal_position,
                distance_to_closest_object=self.distance_to_closest_object,
                flag_goal_reached=self.flag_goal_reached,
                flag_collision=self.flag_collision,
            )

        info = {
            # "reward_dict" : reward_dict,
            "collision": self.flag_collision,
            "goal_reached": self.flag_goal_reached,
            "mean_distance_to_closest_object": np.mean(
                self.distance_to_closest_object_during_episode
            ),
        }

        self.former_orientation_difference_ari_goal = (
            self.orientation_difference_ari_goal
        )
        self.former_distance_to_closest_object = self.distance_to_closest_object
        self.former_position = self.current_position
        self.former_orientation = self.current_orientation

        terminated = self.flag_terminated
        truncated = self.flag_truncated

        # Update the raycast
        self.raycast_distances = deepcopy(self.agent.raycast.raycast_distances)

        (
            _,
            self.distance_to_closest_object,
        ) = compute_nearest_furniture_with_simulation_measurement(self)

        observation = eu.AttrDict()

        for e in self.unsorted_list_of_input:
            if e == "raycast":
                observation.raycast = np.array(self.raycast_distances).astype(
                    dtype=np.float32
                )
            elif e == "distance_to_goal_position":
                observation.distance_to_goal_position = np.array(
                    self.distance_to_goal
                ).astype(dtype=np.float32)
            elif e == "angle_to_goal_position":
                observation.angle_to_goal_position = np.array(
                    self.angle_to_goal
                ).astype(dtype=np.float32)
            else:
                raise UserWarning("Input name : {} is not recognized".format(e))
        return observation, reward, terminated, truncated, info

    def _reset_flags(self):
        self.flag_goal_reached = False
        self.flag_collision = False
        self.flag_truncated = False
        self.flag_terminated = False

    def _check_flags(self):
        self.flag_goal_reached = self._check_goal_reached()
        self.flag_collision = self._check_collision()
        self.flag_terminated = self.flag_goal_reached or self.flag_collision
        self.flag_truncated = self._check_truncated()

    def _check_goal_reached(self, pos=None) -> bool:
        r"""Checks if the agent has reached the goal

        Note : Possibility to provide a position
        Default : pos = self.agent.position

        """
        if pos is None:
            pos = self.agent.position
        distance_to_goal = mpi_sim.utils.measure_center_distance(
            pos, self.goal_position
        )
        if distance_to_goal <= self.config.distance_threshold_goal_reached:
            return True
        else:
            return False

    def _check_collision(self, pos=None) -> bool:
        r"""Checks if the agent has collided with walls or furniture

        Note : Possibility to provide a position
        Default : pos = self.agent.position

        """

        if pos is None:
            pos = self.agent.position

        # Check collision with distance to obstacle
        if self.distance_to_closest_object <= self.config.distance_threshold_collision:
            # self.agent.speech.say(content='Oops, I collided into something')
            # print('Oops, I collided into something')
            return True
        else:
            return False

    def _check_truncated(self) -> bool:
        r"""Checks if the episode if truncated e.g. if the max num of step is reched or if the agent is outside the boundaries

        Note : Possibility to provide a position
        Default : pos = self.agent.position

        """
        if self.cur_step >= self.n_max_steps_per_epsiode or ARI_leaving(
            pos=self.agent.position
        ):
            return True
        else:
            return False

    def _step_simulation(self, linear_velocity, angular_velocity):
        # Reset the measurement variable
        self._reset_flags()

        intermidiate_simulation_step = 0
        while intermidiate_simulation_step < 22:
            # Asynchronization of Simulation and Environment
            self.agent.forward_velocity = linear_velocity
            self.agent.orientation_velocity = angular_velocity

            # Simulation steps
            # self.simulation.run(duration=None, n_steps=1, stop_condition=self.flag_truncated or self.flag_terminated)
            self.simulation.step()
            (
                _,
                self.distance_to_closest_object,
            ) = compute_nearest_furniture_with_simulation_measurement(self)
            self._check_flags()
            if self.flag_truncated or self.flag_terminated:
                break

            intermidiate_simulation_step += 1


from collections import deque


def log_function(
    log,
    env,
    agent,
    step,
    episode,
    step_per_episode,
    transition,
    info,
    exp_info,
    log_state,
    phase,
):
    if (step_per_episode == 1 and episode == 1) and (phase in ["evaluation0"]):
        if "goal_reached" not in log_state:
            log_state.goal_reached = []  # deque(maxlen=exp_info.n_max_episodes_eval)

    if transition.truncated or transition.terminated:
        collision = info["collision"]
        goal_reached = info["goal_reached"]
        mean_distance_to_closest_object = info["mean_distance_to_closest_object"]

        truncated = int(transition.truncated)

        if phase in ["evaluation0"]:
            if goal_reached:
                log_state.goal_reached.append(1)
            else:
                log_state.goal_reached.append(0)

        log.add_value(phase + "/collision", collision, tb_global_step=episode)
        log.add_value(phase + "/goal_reached", goal_reached, tb_global_step=episode)
        log.add_value(phase + "/truncated", truncated, tb_global_step=episode)
        log.add_value(
            phase + "/mean_distance_to_closest_object",
            mean_distance_to_closest_object,
            tb_global_step=episode,
        )

    if phase == "training":
        if episode % exp_info.frequency_saving_network == 0 and step_per_episode == 1:
            if not os.path.exists("./data"):
                os.makedirs("./data")
            if isinstance(agent, SAC):
                agent.save_checkpoint(ckpt_path="data/")


def eval_function(
    log,
    env,
    agent,
    step,
    episode,
    step_per_episode,
    transition,
    info,
    exp_info,
    log_state,
    phase,
):
    if "max_suceed_episode_rate" not in log_state:
        log_state.max_suceed_episode_rate = 0

    if (episode % exp_info.frequency_test == 0) and step_per_episode == 1:
        if "evaluation_env0" in log_state:
            log_state_eval, log = log_state.evaluation_env0.run_evaluation(agent=agent)
            log.save()
