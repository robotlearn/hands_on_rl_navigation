#!/usr/bin/env python
#  type: ignore

# Load packages
import torch.nn as nn
import exputils as eu
import torch
import numpy as np
from hands_on_rl_navigation.networks.utils import *
from hands_on_rl_navigation.networks.utils.layers import *


class PreNavEncoder(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # config for PreNavEncoder
            n_neurons_per_input=16,
            RBF_layer_for_features=True,
            list_of_input_to_encode=[
                ["raycast"],
                ["angle_to_goal_position", "distance_to_goal_position"],
            ],
            RBF_layer_for_raycast=True,
            n_neurons_per_ray=4,
            hidden_dim=512,
            # training_for_navigation_encoder = 'unsupervised', #'unsupervised'
            latent_shape=64,
            normalize_latent_space=1,
        )

    def __init__(self, input_shape, config=None, **kwargs):
        super(PreNavEncoder, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.size_hybrid_input = input_shape.features_shape
        self.size_raycast = input_shape.raycast_shape

        self.n_neurons_per_input = self.config.n_neurons_per_input
        self.n_neurons_per_ray = self.config.n_neurons_per_ray

        self.encode_features = bool(self.size_hybrid_input)
        self.encode_raycast = bool(self.size_raycast)
        self.device = self.config.torch_device

        if self.config.RBF_layer_for_features:
            # Goal coordinate encoder with RBF layer
            self.coord_encoder_fc_rbf = RBFLayer(
                n_in=self.size_hybrid_input,
                n_neurons_per_input=self.n_neurons_per_input,
                is_trainable=True,
            )
        else:
            # Goal coordinate encoder with Linear layer
            self.coord_encoder_fc = nn.Sequential(
                nn.Linear(
                    self.size_hybrid_input,
                    self.size_hybrid_input * self.n_neurons_per_input,
                ),
                nn.ReLU(),
            )

        if self.config.RBF_layer_for_raycast:
            self.raycast_encoder_fc_rbf = RBFLayer(
                n_in=self.size_raycast,
                n_neurons_per_input=self.n_neurons_per_ray,
                is_trainable=True,
            )
        else:
            self.raycast_encoder_fc = nn.Sequential(
                nn.Linear(
                    self.size_raycast, self.size_raycast * self.n_neurons_per_ray
                ),
                nn.ReLU(),
            )

    def _shape_out(self):
        """Compute the size of the output of the PreNavEncoder Network"""

        return (
            self.size_hybrid_input * self.n_neurons_per_input * self.encode_features
            + self.size_raycast * self.n_neurons_per_ray * self.encode_raycast
        )

    def _forward_raycast_encoder(self, state):
        raycast_data = state.raycast
        if (
            np.shape(raycast_data) == (self.size_raycast,)
            or np.shape(raycast_data)[1] != self.size_raycast
        ):
            raise ValueError(
                "The given raycast input has the wrong size. It has a size of {}, but it should be of {}".format(
                    np.shape(raycast_data), (1, self.size_raycast)
                )
            )
        else:
            if self.config.RBF_layer_for_raycast:
                encoded_raycast = self.raycast_encoder_fc_rbf(raycast_data)
            else:
                encoded_raycast = self.raycast_encoder_fc(raycast_data)
        return encoded_raycast

    def _forward_features(self, state):
        features = state.features

        # Check that the given hybrid input has the right size
        if (
            np.shape(features) == (self.size_hybrid_input,)
            or np.shape(features)[1] != self.size_hybrid_input
        ):
            raise ValueError(
                "The given hybrid input has the wrong size. It has a size of {}, but it should be of {}".format(
                    np.shape(features)[1], self.size_hybrid_input
                )
            )
        else:
            if self.config.RBF_layer_for_features:
                encoded_input_features = self.coord_encoder_fc_rbf.forward(features)
            else:
                encoded_input_features = self.coord_encoder_fc(features)
        return encoded_input_features

    def forward(self, state):
        if not self.config.list_of_input_to_encode:
            raise ValueError(
                "There are no input to encode in the list named 'list_of_input_to_encode' "
            )
        else:
            encoded_input = []

            encoded_input_features = self._forward_features(state)
            encoded_input.append(encoded_input_features)

            encoded_raycast = self._forward_raycast_encoder(state)
            encoded_input.append(encoded_raycast)

            encoded_state = torch.cat(encoded_input, dim=1)

        return encoded_state
