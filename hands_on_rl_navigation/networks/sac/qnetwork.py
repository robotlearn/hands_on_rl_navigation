# Load packages
import torch.nn as nn
import exputils as eu
import torch
from hands_on_rl_navigation.networks.utils.networks_utils import *


class QNetwork(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(hidden_dim=512)

    def __init__(
        self, input_shape, num_actions, critic_network=None, config=None, **kwargs
    ):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        super(QNetwork, self).__init__()

        hidden_dim = self.config.hidden_dim

        if critic_network is None:
            # Q1 architecture
            self.linear_q1 = nn.Sequential(
                nn.Linear(input_shape + num_actions, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, 128),
                nn.ReLU(),
                nn.Linear(128, 1),
            )

            # Q2 architecture
            self.linear_q2 = nn.Sequential(
                nn.Linear(input_shape + num_actions, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, 128),
                nn.ReLU(),
                nn.Linear(128, 1),
            )
        else:
            self.linear_q1 = critic_network(input_shape, num_actions)
            self.linear_q2 = critic_network(input_shape, num_actions)

        self.apply(weights_init_)

    def forward(self, state, action):
        xu = torch.cat((state, action), dim=1)

        x1 = self.linear_q1(xu)
        x2 = self.linear_q2(xu)

        return x1, x2
