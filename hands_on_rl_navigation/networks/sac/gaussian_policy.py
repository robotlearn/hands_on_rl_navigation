#!/usr/bin/env python
#  type: ignore

import torch
import torch.nn as nn
import torch.nn.functional as F

from hands_on_rl_navigation.networks.utils.networks_utils import *
import exputils as eu
from torch.distributions import Normal


class GaussianPolicy(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # config for GaussianPolicy class
            hidden_dim=512,
            log_sig_max=2,
            log_sig_min=-20,
            epsilon_log_prob=1e-6,
        )

    def __init__(
        self,
        input_shape,
        num_actions,
        action_space=None,
        config=None,
        actor_network=None,
        **kwargs,
    ):
        super(GaussianPolicy, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        hidden_dim = self.config.hidden_dim

        self.log_sig_max = self.config.log_sig_max
        self.log_sig_min = self.config.log_sig_min
        self.epsilon_log_prob = self.config.epsilon_log_prob

        if actor_network is None:
            self.nav_encoder_fc = nn.Sequential(
                nn.Linear(input_shape, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, 128),
                nn.ReLU(),
                nn.Linear(128, 64),
                nn.ReLU(),
            )
        else:
            self.nav_encoder_fc = actor_network(input_shape)

        self.mean_linear = nn.Linear(self.nav_encoder_fc[-2].out_features, num_actions)
        self.log_std_linear = nn.Linear(
            self.nav_encoder_fc[-2].out_features, num_actions
        )

        self.apply(weights_init_)

        # action rescaling
        if action_space is None:
            self.action_scale = torch.tensor(1.0)
            self.action_bias = torch.tensor(0.0)
        else:
            self.action_scale = torch.FloatTensor(
                (action_space.high - action_space.low) / 2.0
            )
            self.action_bias = torch.FloatTensor(
                (action_space.high + action_space.low) / 2.0
            )

    def forward_given_layer(self, state, id_layer=0):
        r"""Forward the input through a given layer"""
        return self.nav_encoder_fc[:id_layer](state)

    def forward(self, state):
        x = self.nav_encoder_fc(state)
        mean = self.mean_linear(x)
        log_std = self.log_std_linear(x)
        log_std = torch.clamp(log_std, min=self.log_sig_min, max=self.log_sig_max)
        return mean, log_std

    def rsample(self, state):
        mean, log_std = self.forward(state)
        std = log_std.exp()
        # Normal distribution noise
        normal = Normal(mean, std)
        x_t = normal.rsample()  # for reparameterization trick (mean + std * N(0,1))
        y_t = torch.tanh(x_t)
        action = y_t * self.action_scale + self.action_bias
        log_prob = normal.log_prob(x_t)

        # Enforcing Action Bound
        log_prob -= torch.log(
            self.action_scale * (1 - y_t.pow(2)) + self.epsilon_log_prob
        )
        log_prob = log_prob.sum(1, keepdim=True)
        mean = torch.tanh(mean) * self.action_scale + self.action_bias
        return action, log_prob, mean

    def compute_log_prob(self, state, action):
        mean, log_std = self.forward(state)
        std = log_std.exp()
        normal = Normal(mean, std)
        return normal.log_prob(action)

    def to(self, device):
        self.action_scale = self.action_scale.to(device)
        self.action_bias = self.action_bias.to(device)
        return super(GaussianPolicy, self).to(device)
