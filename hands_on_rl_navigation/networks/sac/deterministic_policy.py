import torch
import torch.nn as nn
import torch.nn.functional as F
from hands_on_rl_navigation.networks.utils.networks_utils import *
import exputils as eu


class DeterministicPolicy(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(hidden_dim=512, noise_ratio=0.05)

    def __init__(
        self,
        input_shape,
        num_actions,
        actor_network=None,
        action_space=None,
        config=None,
        **kwargs,
    ):
        super(DeterministicPolicy, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        hidden_dim = self.config.hidden_dim

        if actor_network is None:
            self.nav_encoder_fc = nn.Sequential(
                nn.Linear(input_shape, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, 128),
                nn.ReLU(),
                nn.Linear(128, 64),
                nn.ReLU(),
            )
        else:
            self.nav_encoder_fc = actor_network(input_shape)

        self.mean_linear = nn.Linear(self.nav_encoder_fc[-2].out_features, num_actions)
        self.noise = torch.Tensor(num_actions)

        self.apply(weights_init_)

        # action rescaling
        if action_space is None:
            self.action_scale = 1.0
            self.action_bias = 0.0
        else:
            self.action_scale = torch.FloatTensor(
                (action_space.high - action_space.low) / 2.0
            )
            self.action_bias = torch.FloatTensor(
                (action_space.high + action_space.low) / 2.0
            )

    def forward(self, state):
        x = self.nav_encoder_fc(state)
        mean = torch.tanh(self.mean_linear(x)) * self.action_scale + self.action_bias
        return mean

    def rsample(self, state):
        mean = self.forward(state)
        noise = self.noise.normal_(0.0, std=0.1)
        noise = noise.clamp(-self.config.noise_ratio, self.config.noise_ratio)
        action = mean + noise
        return action, torch.tensor(0.0), mean

    def to(self, device):
        self.action_scale = self.action_scale.to(device)
        self.action_bias = self.action_bias.to(device)
        self.noise = self.noise.to(device)
        return super(DeterministicPolicy, self).to(device)
