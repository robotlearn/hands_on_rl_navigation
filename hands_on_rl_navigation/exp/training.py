import numpy as np
import exputils as eu
import exputils.data.logging as log
import gymnasium as gym
import time
from copy import copy
from collections import namedtuple
from tqdm import trange
from collections import deque

Transition = namedtuple(
    "Experience",
    field_names=[
        "observation",
        "action",
        "reward",
        "next_observation",
        "terminated",
        "truncated",
    ],
)


def run_training(config=None, agent=None, env=None, **kwargs):
    """
    Runs the RL training for the given environment and Off-policy agent.

    Call with run_training(config='get_default_config') to retrieve default config.

    :param config: Configuration:
        TODO
    :return: log of the rl training.

    Configuration:
        agent: Agent object or a exputils class caller config dict.
        env: Gym environment object, name of gym environment or a exputils class caller config dict.
        seed: Integer seed used for the training. See exputils.misc.seed fuction. (default=None)
        is_update_agent: Should the agents update function be called after each step. (default=True)
    """

    ###############
    # configuration

    default_config = eu.AttrDict(
        seed=None,
        env=None,
        agent=None,
        n_max_steps=np.inf,
        n_max_episodes=np.inf,
        is_update_agent=True,
        log_functions=[],
        log_state=None,
        default_exp_info=eu.AttrDict(frequency_saving_network=100, frequency_test=50),
        log_name_episode="episode",
        log_name_step="step",
        log_name_step_per_episode="step_per_episode",
        log_name_episode_per_step="episode_per_step",
        log_name_reward_per_episode="reward_per_episode",
        log_name_reward_per_step="reward_per_step",
        log_name_total_reward="total_reward",
        log_name_time="time",
        log_name_episode_time="episode_time",
        log_tensorboard=None,
    )

    # use copy mode 'copy' so that agents and environments that ar given are not deep copied
    # this results in a call by reference and not call by value behavior for these elements
    config = eu.combine_dicts(kwargs, config, default_config, copy_mode="copy")

    if config.env is None:
        raise ValueError("Environment must be defined!")

    if config.agent is None:
        raise ValueError("Agent must be defined!")

    if config.n_max_episodes == np.inf and config.n_max_steps == np.inf:
        raise ValueError("Configuration n_max_episodes or n_max_steps must not be inf.")

    ##############
    # preparation

    eu.misc.seed(config)

    # internally log-functions are a list
    log_functions = config.log_functions
    if not isinstance(log_functions, list):
        log_functions = [log_functions]

    log_state = config.log_state

    # get env
    if env is None:
        if isinstance(config.env, dict):
            env = eu.misc.create_object_from_config(config.env)
        elif isinstance(config.env, str):
            env = gym.make(config.env)
        else:
            env = config.env

    # get agent
    if agent is None:
        memory = kwargs.pop("memory", None)  # If memory is in the kwargs
        config.agent.seed = config.seed
        if isinstance(config.agent, dict):
            agent = eu.misc.create_object_from_config(
                config=config.agent, env=env.config, memory=memory
            )
        else:
            agent = config.agent

    ##############
    # execution

    start_time_in_sec = time.time()

    total_reward = 0
    step = 0
    episode = 0

    for episode in trange(1, config.n_max_episodes + 1):
        # log_state.goal_reached_ratio = 0
        # while episode < config.n_max_episodes+1 or log_state.goal_reached_ratio < 0.95:
        start_episode_time_in_sec = time.time()

        reward_per_episode = 0
        step_per_episode = 0

        obs, info = env.reset()

        # logging
        # log.add_value(config.log_name_episode, episode)

        # step over episode
        done = False
        while not done:  # and step_per_episode <= config.env.n_max_steps_per_episode and step < config.n_max_steps:
            exp_info = copy(config.default_exp_info)
            exp_info.step = step
            exp_info.episode = episode
            exp_info.step_per_episode = step_per_episode
            exp_info.log = log

            action = agent.get_action(obs, info, exp_info)

            prev_obs = obs
            obs, reward, terminated, truncated, info = env.step(action)

            #  give agents attr dicts as info if possible
            if isinstance(info, dict):
                try:
                    info = eu.AttrDict(info)
                finally:
                    pass

            transition = Transition(
                observation=prev_obs,
                action=action,
                reward=reward,
                next_observation=obs,
                terminated=terminated,
                truncated=truncated,
            )

            if config.is_update_agent:
                agent.update(transition, exp_info)

            reward_per_episode += reward
            total_reward += reward
            step += 1
            step_per_episode += 1

            # logging

            log.add_value(
                config.log_name_reward_per_step,
                reward,
                log_to_tb=config.log_tensorboard,
                tb_global_step=step,
            )
            # log.add_value(config.log_name_episode_per_step, episode)
            exp_info.info = info
            # additional logging if exists
            for log_func in log_functions:
                log_func(
                    log,
                    env=env,
                    agent=agent,
                    step=step,
                    episode=episode,
                    step_per_episode=step_per_episode,
                    transition=transition,
                    info=info,
                    exp_info=exp_info,
                    log_state=log_state,
                    phase="training",
                )

            done = terminated or truncated
        log.add_value(config.log_name_step_per_episode, step_per_episode)
        end_episode_time_in_sec = time.time()
        log.add_value(
            config.log_name_episode_time,
            end_episode_time_in_sec - start_episode_time_in_sec,
        )

        log.add_value(
            config.log_name_reward_per_episode,
            reward_per_episode,
            log_to_tb=config.log_tensorboard,
            tb_global_step=episode,
        )

        if episode % 100 == 0:
            log.save()

        # episode += 1

    # env.close()

    # log elapsed time
    end_time_in_sec = time.time()
    log.add_value(config.log_name_time, end_time_in_sec - start_time_in_sec)

    # log.add_value(config.log_name_total_reward, total_reward)

    log.save()
    env.simulation.close()
    agent.save_checkpoint(ckpt_path="checkpoints/final_")

    return log, agent, env
