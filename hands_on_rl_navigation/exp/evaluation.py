import numpy as np
import exputils as eu
import exputils.data.logging as log
import gymnasium as gym
import time
from copy import copy
from collections import namedtuple
from tqdm import trange


class Evaluation:
    @staticmethod
    def default_config():
        return eu.AttrDict(
            seed=None,
            env=None,
            n_max_steps=np.inf,
            n_max_episodes=np.inf,
            n_max_episodes_eval=1,
            is_update_agent=True,
            log_functions=[],
            log_state=eu.AttrDict(),
            default_exp_info=eu.AttrDict(),
            log_name_episode_eval="episode_eval",
            log_name_step_eval="step_eval",
            log_name_step_per_episode_eval="step_per_episode_eval",
            log_name_episode_per_step_eval="episode_per_step_eval",
            log_name_reward_per_episode_eval="reward_per_episode_eval",
            log_name_reward_per_step_eval="reward_per_step_eval",
            log_name_total_reward_eval="total_reward_eval",
            log_name_time_eval="time_eval",
            log_name_episode_time_eval="episode_time_eval",
            log_tensorboard=None,
        )

    def __init__(
        self,
        config=None,
        eval_id=0,
        config_eval=None,
        n_max_episode_eval=None,
        **kwargs,
    ):
        """Set an evaluation loop by taking an agent, an Id and two configurqation dictionnaries"""
        self.eval_id = eval_id

        # use copy mode 'copy' so that agents and environments that ar given are not deep copied
        # this results in a call by reference and not call by value behavior for these elements
        config = eu.combine_dicts(
            kwargs, config, self.default_config(), copy_mode="copy"
        )

        if config.env is None:
            raise ValueError("Environment must be defined for evaluation!")

        if config.agent is None:
            raise ValueError("Agent must be defined for evaluation!")

        if config.n_max_episodes == np.inf and config.n_max_steps == np.inf:
            raise ValueError(
                "Configuration n_max_episodes or n_max_steps must not be inf for evaluation."
            )

        ##############
        # preparation

        eu.misc.seed(config)

        # internally log-functions are a list
        self.log_functions = config.log_functions
        if not isinstance(self.log_functions, list):
            self.log_functions = [self.log_functions]

        # Modif config.env w.r.t the config of evaluation
        config.env = eu.combine_dicts(kwargs, config_eval, config.env)

        # get env
        if isinstance(config.env, dict):
            self.env = eu.misc.create_object_from_config(config.env)
        elif isinstance(config.env, str):
            self.env = gym.make(config.env)
        else:
            self.env = config.env

        self.Transition = namedtuple(
            "Experience",
            field_names=[
                "observation",
                "action",
                "reward",
                "next_observation",
                "terminated",
                "truncated",
            ],
        )

        self.log_state = config.log_state

        if "eval_id" not in self.log_state:
            self.log_state.eval_id = self.eval_id

        self.config = config
        if not n_max_episode_eval:
            self.n_max_episode_eval = self.config.n_max_episodes_eval
        else:
            self.n_max_episode_eval = n_max_episode_eval

    def run_evaluation(self, agent):
        """
        Runs the evaluation for the given environment and agent.

        Call with run_evaluation(config='get_default_config') to retrieve default config.

        :param config: Configuration:
            TODO
        :return: log of the rl training.

        Configuration:
            agent: Agent object or a exputils class caller config dict.
            env: Gym environment object, name of gym environment or a exputils class caller config dict.
            seed: Integer seed used for the training. See exputils.misc.seed fuction. (default=None)
            is_update_agent: Should the agents update function be called after each step. (default=True)
        """

        ##############
        # execution
        env = self.env
        start_time_in_sec = time.time()

        total_reward = 0
        step = 0
        episode = 0

        for episode in range(1, self.n_max_episode_eval + 1):
            start_episode_time_in_sec = time.time()

            reward_per_episode = 0
            step_per_episode = 0

            obs, info = env.reset()

            # logging
            log.add_value(self.config.log_name_episode_eval, episode)

            # step over episode
            done = False
            while not done:  # and step_per_episode <= self.config.env.n_max_steps_per_episode and step < self.config.n_max_steps:
                exp_info = copy(self.config.default_exp_info)
                exp_info.step = step
                exp_info.episode = episode
                exp_info.step_per_episode = step_per_episode
                exp_info.n_max_episodes_eval = self.config.n_max_episodes_eval

                action = agent.get_action(obs, info, exp_info, evaluate=True)

                prev_obs = obs
                obs, reward, terminated, truncated, info = env.step(action)

                #  give agents attr dicts as info if possible
                if isinstance(info, dict):
                    try:
                        info = eu.AttrDict(info)
                    finally:
                        pass

                transition = self.Transition(
                    observation=prev_obs,
                    action=action,
                    reward=reward,
                    next_observation=obs,
                    terminated=terminated,
                    truncated=truncated,
                )

                reward_per_episode += reward
                total_reward += reward
                step += 1
                step_per_episode += 1

                # logging
                # log.add_value(config.log_name_step, step)

                # log.add_value(self.config.log_name_reward_per_step_eval+str(self.eval_id), reward, log_to_tb=self.config.log_tensorboard, tb_global_step=step)
                # log.add_value(self.config.log_name_episode_per_step_eval+str(self.eval_id), episode)

                # additional logging if exists
                for log_func in self.log_functions:
                    log_func(
                        log,
                        env=env,
                        agent=agent,
                        step=step,
                        episode=episode,
                        step_per_episode=step_per_episode,
                        transition=transition,
                        info=info,
                        exp_info=exp_info,
                        log_state=self.log_state,
                        phase="evaluation" + str(self.eval_id),
                    )

                done = terminated or truncated
            end_episode_time_in_sec = time.time()
            # log.add_value(self.config.log_name_episode_time_eval+str(self.eval_id), end_episode_time_in_sec - start_episode_time_in_sec)
            log.add_value(
                self.config.log_name_step_per_episode_eval + str(self.eval_id),
                step_per_episode,
            )
            log.add_value(
                self.config.log_name_reward_per_episode_eval + str(self.eval_id),
                reward_per_episode,
                log_to_tb=self.config.log_tensorboard,
                tb_global_step=episode,
            )

        env.close()
        # log elapsed time
        # end_time_in_sec = time.time()
        # log.add_value(self.config.log_name_time_eval, end_time_in_sec - start_time_in_sec)

        # log.add_value(self.config.log_name_total_reward_eval, total_reward)

        return self.log_state, log
