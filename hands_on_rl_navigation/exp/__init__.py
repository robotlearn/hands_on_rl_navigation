from hands_on_rl_navigation.exp.training import run_training
from hands_on_rl_navigation.exp.evaluation import Evaluation

__version__ = "0.0.1"
