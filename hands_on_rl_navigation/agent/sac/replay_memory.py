#!/usr/bin/env python
#  type: ignore

# Load packages
import numpy as np
from collections import deque, namedtuple
import torch
import random
import exputils as eu
import torch.nn as nn


class ExperienceReplay:
    @staticmethod
    def default_config():
        return eu.AttrDict(buffer_size=int(1e5), buffer_type="simple", nb_frame=1)

    def __init__(self, device, config, **kwargs):
        super(ExperienceReplay, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # General parameters for buffer
        self.device = device
        self.buffer_size = self.config.buffer_size
        self.memory = deque(maxlen=self.config.buffer_size)
        self.batch_size = self.config.batch_size
        self.unsorted_list_of_input = []
        for elt in self.config.list_of_input_to_memorize:
            if isinstance(elt, str):
                self.unsorted_list_of_input.append(elt)
            else:
                self.unsorted_list_of_input.extend(elt)

    def import_memory(self, memory):
        self.memory = memory

    def clear(self):
        self.memory.clear()

    def add(
        self,
        experience: namedtuple(
            "Experience",
            field_names=[
                "observation",
                "action",
                "reward",
                "next_observation",
                "terminated",
                "truncated",
            ],
        ),
    ):
        if experience is not None:
            data_to_pop = [
                data
                for data in experience.observation
                if data not in self.unsorted_list_of_input
            ]
            for data in data_to_pop:
                experience.observation.pop(data)
                experience.next_observation.pop(data)

            self.memory.append(experience)
            # if experience.terminated or experience.truncated :
            #     import matplotlib.pyplot as plt
            #     fig, ax = plt.subplots(figsize=(4,4), nrows = 1, ncols = 1)
            #     plt.ion()
            #     for id, exp in enumerate(self.memory) :
            #         ax.imshow(np.flip(exp.observation.grid.T, axis=0), cmap= 'Greys')
            #         ax.set_title('exp {} pixel ratio {}'.format(id, np.count_nonzero(exp.observation.grid) / np.shape(exp.observation.grid.flatten())[0]))
            #         plt.pause(0.5)
            #     plt.close()
        else:
            pass

    def len(self):
        return self.memory.__len__()

    def sample(self):
        experiences = random.sample(self.memory, k=self.batch_size)

        observations = eu.AttrDict()
        next_observations = eu.AttrDict()
        for data in self.unsorted_list_of_input:
            if "grid" in data:
                observations[data] = (
                    torch.from_numpy(
                        np.vstack(
                            [
                                e.observation[data][np.newaxis, :]
                                for e in experiences
                                if e is not None
                            ]
                        )
                    )
                    .float()
                    .to(self.device)
                )

                next_observations[data] = (
                    torch.from_numpy(
                        np.vstack(
                            [
                                e.next_observation[data][np.newaxis, :]
                                for e in experiences
                                if e is not None
                            ]
                        )
                    )
                    .float()
                    .to(self.device)
                )
            elif data == "raycast":
                observations[data] = (
                    torch.from_numpy(
                        np.vstack(
                            [
                                e.observation[data].flatten()[np.newaxis, :]
                                for e in experiences
                                if e is not None
                            ]
                        )
                    )
                    .float()
                    .to(self.device)
                )

                next_observations[data] = (
                    torch.from_numpy(
                        np.vstack(
                            [
                                e.next_observation[data].flatten()[np.newaxis, :]
                                for e in experiences
                                if e is not None
                            ]
                        )
                    )
                    .float()
                    .to(self.device)
                )

            elif "features" not in observations:
                if self.config.nb_frame > 1:
                    observations.features = (
                        torch.from_numpy(
                            np.vstack(
                                [
                                    np.array(
                                        [
                                            e.observation[input][elt]
                                            for elt in range(self.config.nb_frame)
                                            for input in self.unsorted_list_of_input
                                            if "grid" not in input or input != "raycast"
                                        ]
                                    )[np.newaxis, :]
                                    for e in experiences
                                    if e is not None
                                ]
                            )
                        )
                        .float()
                        .to(self.device)
                    )

                    next_observations.features = (
                        torch.from_numpy(
                            np.vstack(
                                [
                                    np.array(
                                        [
                                            e.next_observation[input][elt]
                                            for elt in range(self.config.nb_frame)
                                            for input in self.unsorted_list_of_input
                                            if "grid" not in input or input != "raycast"
                                        ]
                                    )[np.newaxis, :]
                                    for e in experiences
                                    if e is not None
                                ]
                            )
                        )
                        .float()
                        .to(self.device)
                    )
                else:
                    observations.features = (
                        torch.from_numpy(
                            np.vstack(
                                [
                                    np.array(
                                        [
                                            e.observation[input]
                                            for input in self.unsorted_list_of_input
                                            if "grid" not in input
                                            and input != "raycast"
                                        ]
                                    )[np.newaxis, :]
                                    for e in experiences
                                    if e is not None
                                ]
                            )
                        )
                        .float()
                        .to(self.device)
                    )

                    next_observations.features = (
                        torch.from_numpy(
                            np.vstack(
                                [
                                    np.array(
                                        [
                                            e.next_observation[input]
                                            for input in self.unsorted_list_of_input
                                            if "grid" not in input
                                            and input != "raycast"
                                        ]
                                    )[np.newaxis, :]
                                    for e in experiences
                                    if e is not None
                                ]
                            )
                        )
                        .float()
                        .to(self.device)
                    )

        actions = (
            torch.from_numpy(
                np.vstack([e.action for e in experiences if e is not None])
            )
            .float()
            .to(self.device)
        )
        rewards = (
            torch.from_numpy(
                np.vstack([e.reward for e in experiences if e is not None])
            )
            .float()
            .to(self.device)
        )
        terminateds = (
            torch.from_numpy(
                np.vstack([e.terminated for e in experiences if e is not None]).astype(
                    np.uint8
                )
            )
            .float()
            .to(self.device)
        )
        truncateds = (
            torch.from_numpy(
                np.vstack([e.truncated for e in experiences if e is not None]).astype(
                    np.uint8
                )
            )
            .float()
            .to(self.device)
        )

        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(figsize=(4,4), nrows = 1, ncols = 1)
        # plt.ion()
        # for id, exp in enumerate(observations.grid) :
        #     ax.imshow(np.flip(np.array(exp[0,:,:]).T, axis=0), cmap= 'Greys')
        #     ax.set_title('exp {}'.format(id))
        #     plt.pause(0.5)
        # plt.close()

        return (
            observations,
            actions,
            rewards,
            next_observations,
            terminateds,
            truncateds,
        )

    def to(self, device):
        return super(ExperienceReplay, self).to(device)
