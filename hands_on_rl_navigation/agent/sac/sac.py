#!/usr/bin/env python
#  type: ignore

import os
import torch
import torch.nn.functional as F
from torch.optim import Adam
from hands_on_rl_navigation.networks.utils.networks_utils import (
    soft_update,
    hard_update,
)
from hands_on_rl_navigation.networks.sac import (
    GaussianPolicy,
    QNetwork,
    DeterministicPolicy,
)
from hands_on_rl_navigation.agent.agent import Agent
from hands_on_rl_navigation.networks.utils import *
import exputils as eu
import numpy as np
from hands_on_rl_navigation.networks.utils.pre_nav_encoders.pre_nav_encoder import (
    PreNavEncoder,
)
from hands_on_rl_navigation.agent.sac.replay_memory import ExperienceReplay


class SAC(Agent):
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # Learning hyperparameters
            batch_size=1024,
            discount_factor=0.99,
            soft_target_update=0.005,  # Target smoothing coefficient(τ)
            learning_rate=0.005,
            network_update=50,  # Number of steps between each backprop
            nb_gradient_steps=20,  # Number of backprop per update step
            start_learning=1,  # Threshold under which the learning has to start
            # Replay buffer hyperparameter
            buffer_size=int(1e5),
            buffer_type="simple",
            # Policy Type: Gaussian | Deterministic
            policy_type="Gaussian",
            # Temperature parameter α determines the relative importance of the entropy term against the reward
            alpha=1,
            # Automatically adjust α
            automatic_entropy_tuning=False,
            alpha_dynamic=0,  # 0.995,
            alpha_min=0.2,
            start_alpha_dynamic=2500,
            # Device
            device="gpu",
            # Navigation Encoder parameters
            list_of_input_to_encode=[
                ["raycast"],
                ["angle_to_goal_position", "distance_to_goal_position"],
            ],
            list_of_input_to_memorize=[
                ["raycast"],
                ["angle_to_goal_position", "distance_to_goal_position"],
            ],
            # Parameters for actor network
            log_sig_max=2,
            log_sig_min=-20,
            epsilon_log_prob=1e-6,
            action_sampling="",
            with_value_network=False,
            noise_ratio=0.05,  # for deterministic policy
            nav_encoder_fc=None,
        )

    def __init__(self, env=None, import_weight=None, config=None, **kwargs):
        super(SAC, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # Entropy term
        self.alpha = self.config.alpha
        self.policy_type = self.config.policy_type
        self.automatic_entropy_tuning = self.config.automatic_entropy_tuning
        self.network_update = self.config.network_update
        self.flag_start_learning = 0
        self.t_step = 0
        self.t_update = 0

        self.device = self.device_selection(self.config)
        self.config.torch_device = self.device

        # Check that we memorize the inputs we want to encode :
        for elt in self.config.list_of_input_to_encode:
            if elt not in self.config.list_of_input_to_memorize:
                raise UserWarning(
                    "The input : {} has to be encoded but it is not in the memory"
                )
        # corresponds to num of variable to control

        if "nb_frame" in env:
            # frame mode
            self.frame_mode = True
            self.nb_frame = env.grid_shape[0]

        else:
            self.frame_mode = False
            self.nb_frame = 1
        self.config.nb_frame = self.nb_frame
        action_space = eu.AttrDict(
            size=2,
            low=np.array([env.vmin, env.wmin]),  # v_min / w_min
            high=np.array([env.vmax, env.wmax]),  # v_max / w_max
        )

        self.raycast_field_of_view = env.raycast_field_of_view
        self.raycast_angle_between_two_ray = env.raycast_angle_between_two_ray
        self.num_input_fc_net = 0

        # Create the list of input labels
        self.unsorted_list_of_input = []
        for elt in self.config.list_of_input_to_encode:
            if isinstance(elt, str):
                self.unsorted_list_of_input.append(elt)
            else:
                self.unsorted_list_of_input.extend(elt)

        encode_raycast = False
        for label in self.unsorted_list_of_input:
            if label == "raycast":
                encode_raycast = True
            else:
                self.num_input_fc_net += 1
        input_shape = eu.AttrDict(
            raycast_shape=self.nb_frame
            * (int(self.raycast_field_of_view / self.raycast_angle_between_two_ray) + 1)
            * encode_raycast,
            features_shape=self.nb_frame * self.num_input_fc_net,
        )
        # State preEncoder
        self.navigation_encoder = PreNavEncoder(
            input_shape=input_shape, config=self.config
        ).to(self.device)
        encoded_state_shape = self.navigation_encoder._shape_out()

        self.critic = QNetwork(
            encoded_state_shape,
            action_space.size,
            critic_network=self.config.agent_critic_network,
            config=self.config,
        ).to(device=self.device)
        self.critic_target = QNetwork(
            encoded_state_shape,
            action_space.size,
            critic_network=self.config.agent_critic_network,
            config=self.config,
        ).to(self.device)
        hard_update(self.critic_target, self.critic)

        if self.policy_type == "Gaussian":
            # Target Entropy = −dim(A) (e.g. , -6 for HalfCheetah-v2) as given in the paper
            if self.automatic_entropy_tuning:
                self.target_entropy = -torch.prod(
                    torch.Tensor(action_space.size).to(self.device)
                ).item()
                self.log_alpha = torch.zeros(1, requires_grad=True, device=self.device)
                self.alpha_optim = Adam([self.log_alpha], lr=self.config.learning_rate)
            self.actor = GaussianPolicy(
                encoded_state_shape,
                action_space.size,
                action_space,
                actor_network=self.config.agent_actor_network,
                config=self.config,
            ).to(self.device)

        elif self.policy_type == "Deterministic":
            self.alpha = 0
            self.automatic_entropy_tuning = False
            self.actor = DeterministicPolicy(
                encoded_state_shape,
                action_space.size,
                action_space,
                actor_network=self.config.agent_actor_network,
                config=self.config,
            ).to(self.device)

        else:
            raise ValueError("The policy type was not recognized.")

        self.optimizer = Adam(
            [
                {"params": self.actor.parameters()},
                {"params": self.navigation_encoder.parameters()},
                {"params": self.critic.parameters()},
            ],
            lr=self.config.learning_rate,
        )

        self.buffer = ExperienceReplay(self.device, self.config)

        self.flag_memory_imported = False
        self.entropy_per_episode = []
        self.current_episode = None

        if import_weight:
            self.navigation_encoder.load_state_dict(
                state_dict=torch.load(
                    import_weight.navigation_encoder, map_location=self.device
                )
            )
            self.actor.load_state_dict(
                state_dict=torch.load(import_weight.actor, map_location=self.device)
            )
            try:
                self.critic.load_state_dict(
                    state_dict=torch.load(
                        import_weight.critic, map_location=self.device
                    )
                )
            except:
                pass

    def _create_input(self, observation):
        r"""Set an observation as a tensor for the neural network"""
        input = eu.AttrDict()
        features = []
        for e in self.unsorted_list_of_input:
            if "grid" in e:
                input[e] = (
                    torch.from_numpy(observation[e])
                    .float()
                    .unsqueeze(0)
                    .to(self.device)
                )
            elif e == "raycast":
                input.raycast = (
                    torch.from_numpy(observation.raycast)
                    .float()
                    .flatten()
                    .unsqueeze(0)
                    .to(self.device)
                )
            else:
                if np.shape(observation[e]):
                    for elt in observation[e]:
                        features.append(elt)
                else:
                    features.append(observation[e])

        input.features = (
            torch.from_numpy(np.array(features)).float().unsqueeze(0).to(self.device)
        )
        return input

    def predict_action(self, observation, info=None, exp_info=None):
        r"""Evaluates the agent on a given observation"""
        return self.get_action(observation, info, exp_info, evaluate=True)

    def get_action(self, observation, info, exp_info, evaluate=False):
        r"""Predict an action giving an observation"""
        input = self._create_input(observation)
        encoded_input = self.navigation_encoder(input)

        if evaluate is False:
            if self.current_episode is None:
                self.current_episode = exp_info.episode

            if self.current_episode != exp_info.episode:
                exp_info.log.add_value(
                    "data/entropy_per_episode",
                    np.mean(self.entropy_per_episode),
                    tb_global_step=exp_info.episode,
                )
                self.entropy_per_episode = []
                self.current_episode = exp_info.episode

            action, log_prob, _ = self.actor.rsample(encoded_input)
            if not os.path.exists("./data"):
                os.makedirs("./data")
            self.entropy_per_episode.append(log_prob.detach().cpu().item())

            if self.automatic_entropy_tuning:
                if isinstance(self.alpha, torch.Tensor):
                    exp_info.log.add_value(
                        "data/alpha",
                        self.alpha.detach().cpu().item(),
                        tb_global_step=exp_info.episode,
                    )
                else:
                    exp_info.log.add_value(
                        "data/alpha", self.alpha, tb_global_step=exp_info.episode
                    )
        else:
            _, _, action = self.actor.rsample(encoded_input)
        return tuple(action.detach().cpu().numpy()[0])

    def get_q_value(
        self, observation, action, info=None, exp_info=None, evaluate=False
    ):
        r"""Predict the Q-value related to a state-action pair with the critic network"""

        input = self._create_input(observation)
        self.navigation_encoder.eval()
        encoded_input = self.navigation_encoder(input)
        self.critic.eval()

        # Check that 'action' is a tensor
        if not isinstance(action, torch.Tensor):
            action = torch.from_numpy(np.array(action))

        qf1, qf2 = self.critic(encoded_input, action)

        self.navigation_encoder.train()
        self.critic.train()

        return torch.min(qf1, qf2).detach().cpu().numpy()[0]

    def get_entropy(self, observation):
        r"""Predict the log entropy related to a state-action pair with the critic network"""
        input = self._create_input(observation)
        encoded_input = self.navigation_encoder(input)
        _, log_prob, _ = self.actor.rsample(encoded_input)

        return log_prob.detach().cpu().numpy()

    def update(self, transition, exp_info=eu.AttrDict()):
        r"""Updates the agent"""
        self.remember(transition)
        self._train(exp_info=exp_info)

    def remember(self, transition):
        """Stores the experience in the experience replay buffer"""
        self.buffer.add(transition)

    def _train(self, exp_info=None):
        """Train the Q-network with policy and target networks"""
        self.t_step = self.t_step + 1
        if not os.path.exists("./data"):
            os.makedirs("./data")
        # Learning is done only self.network_update steps for being detach
        # from the sensibility of the environment
        # Set a parameter to set the nb of epoch

        if (self.t_step % self.network_update) == 0:
            # checking if the memory has enough experience
            if self.buffer.len() >= max(
                self.config.start_learning, self.config.batch_size
            ):
                if not self.flag_start_learning:
                    # print("Learning is starting")
                    self.flag_start_learning += 1
                # nb_gradient_steps = min(int(self.buffer.len()/self.config.batch_size), self.config.nb_gradient_steps)
                for _ in range(self.config.nb_gradient_steps):
                    qf1_loss, qf2_loss, actor_loss, min_qf_pi, log_pi = self._learn()
                    self.t_update += 1
                    if exp_info:
                        exp_info.log.add_value(
                            "data/loss_qf1", qf1_loss, tb_global_step=exp_info.episode
                        )
                        exp_info.log.add_value(
                            "data/loss_qf2", qf2_loss, tb_global_step=exp_info.episode
                        )
                        exp_info.log.add_value(
                            "data/loss_actor",
                            actor_loss,
                            tb_global_step=exp_info.episode,
                        )
                        exp_info.log.add_value(
                            "data/loss_actor_q_function",
                            min_qf_pi,
                            tb_global_step=exp_info.episode,
                        )
                        exp_info.log.add_value(
                            "data/loss_actor_entropy",
                            log_pi,
                            tb_global_step=exp_info.episode,
                        )

        exp_info.log.add_value(
            "data/length_buffer", self.buffer.len(), tb_global_step=exp_info.episode
        )

    def _learn(self):
        (
            observations,
            actions,
            rewards,
            next_observations,
            terminateds,
            truncateds,
        ) = self.buffer.sample()

        ### Update of the critic ###
        with torch.no_grad():
            encoded_next_observations = self.navigation_encoder(next_observations)
            # Prediction of the next action (a') and log(π) from the Actor
            next_action, next_state_log_pi, _ = self.actor.rsample(
                encoded_next_observations
            )
            # Prediciton of the Q value associated to a' and s' with target network of critic
            qf1_next_target, qf2_next_target = self.critic_target(
                encoded_next_observations, next_action
            )
            # Minimizing the overestimation of the q value and adding the entropy term w.r.t alpha factor
            min_qf_next_target = (
                torch.min(qf1_next_target, qf2_next_target)
                - self.alpha * next_state_log_pi
            )
            # Estimation of next q value with : td error + entropy term
            next_q_value = rewards + (
                self.config.discount_factor * min_qf_next_target * (1 - terminateds)
            )

        # Foward pass with the navigation encoder of the actor
        encoded_observations = self.navigation_encoder(observations)

        # Prediciton of the Q value associated to a' and 's' with policy network of critic
        qf1, qf2 = self.critic(
            encoded_observations, actions
        )  # Two Q-functions to mitigate positive bias in the policy improvement step
        qf1_loss = F.mse_loss(
            qf1, next_q_value
        )  # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        qf2_loss = F.mse_loss(
            qf2, next_q_value
        )  # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        qf_loss = qf1_loss + qf2_loss

        ### Actor Loss ###
        # Prediction of the action (a) and log(π) from the Actor
        actions_from_actor, log_pi, _ = self.actor.rsample(encoded_observations)
        # Prediciton of the Q value associated to a and s with policy network of critic
        qf1_pi, qf2_pi = self.critic(encoded_observations, actions_from_actor)
        min_qf_pi = torch.min(qf1_pi, qf2_pi)

        actor_loss = (
            (self.alpha * log_pi) - min_qf_pi
        ).mean()  # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]

        if self.automatic_entropy_tuning:
            alpha_loss = -(
                self.log_alpha * (log_pi + self.target_entropy).detach()
            ).mean()

            self.alpha_optim.zero_grad()
            alpha_loss.backward()
            self.alpha_optim.step()

            self.alpha = self.log_alpha.exp()

        elif self.config.alpha_dynamic:
            alpha_loss = torch.tensor(0.0).to(self.device)
            if self.t_update >= self.config.start_alpha_dynamic:
                self.alpha = max(
                    self.config.alpha_dynamic * self.alpha, self.config.alpha_min
                )

        loss = actor_loss + qf_loss
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        soft_update(self.critic_target, self.critic, self.config.soft_target_update)
        return (
            qf1_loss.item(),
            qf2_loss.item(),
            actor_loss.item(),
            (-min_qf_pi).mean().item(),
            log_pi.mean().item(),
        )

    # Save model parameters
    def save_checkpoint(self, suffix="", ckpt_path=None, full=False, episode=None):
        if not os.path.exists("checkpoints/"):
            os.makedirs("checkpoints/")
        if ckpt_path is None:
            ckpt_path = "checkpoints/sac_checkpoint_{}".format(suffix)

        if episode is not None:
            torch.save(
                self.actor.state_dict(),
                ckpt_path + "/checkpoint_{}_state_dict.pt".format(episode),
            )
            torch.save(
                self.navigation_encoder.state_dict(),
                ckpt_path + "/checkpoint_{}_nav_encoder_state_dict.pt".format(episode),
            )
        else:
            torch.save(self.actor.state_dict(), ckpt_path + "actor_state_dict.pt")
            torch.save(
                self.navigation_encoder.state_dict(),
                ckpt_path + "nav_encoder_state_dict.pt",
            )
            torch.save(self.critic.state_dict(), ckpt_path + "critic_state_dict.pt")
