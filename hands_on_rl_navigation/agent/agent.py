#!/usr/bin/env python
#  type: ignore

# Load packages
import torch
import exputils as eu


class Agent(object):
    @staticmethod
    def default_config():
        return eu.AttrDict()

    def __init__(self, env=None, import_weight=None, config=None, **kwargs):
        """This class contains the mandatory function an agent must have"""
        self.snaps = []

    def device_selection(self, config):
        """Select a device between CPU and GPU depending on availability"""
        if config.device == "gpu" or config.device == "cuda":
            return torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        elif config.device == "cpu":
            return torch.device("cpu")
        else:
            return "Error : Device name not recognized"

    def get_action(self, observation, info, exp_info, evaluate=False):
        """Take an observation and take an action."""
        raise NotImplementedError

    def predict_action(self, observation, info=None, exp_info=None):
        raise NotImplementedError

    def update(self, transition, exp_info=eu.AttrDict()) -> None:
        """Add to the buffer the transition and perform train the Q-Network if there is enough transition"""
        raise NotImplementedError

    # Functions to study memory allocation

    def profiler_snapshot(self):
        import tracemalloc

        self.snaps.append(tracemalloc.take_snapshot())

    def profiler_display_stats(self, episode):
        stats = self.snaps[0].statistics("filename")
        print(
            "\n Episode :" + str(episode), "\n*** top 5 stats grouped by filename ***"
        )
        for s in stats[:5]:
            print(s)

    def profiler_compare(self, episode):
        first = self.snaps[0]
        stats = self.snaps[1:][-1].compare_to(first, "lineno")
        print("\n Episode :" + str(episode), "\n*** top 10 stats ***")
        for s in stats[:10]:
            print(s)

    def profiler_print_trace(self, episode):
        import tracemalloc

        # pick the last saved snapshot, filter noise
        snapshot = self.snaps[-1].filter_traces(
            (
                tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
                tracemalloc.Filter(False, "<frozen importlib._bootstrap_external>"),
                tracemalloc.Filter(False, "<unknown>"),
            )
        )
        largest = snapshot.statistics("traceback")[0]

        print(
            "\n Episode :" + str(episode),
            f"\n*** Trace for largest memory block - ({largest.count} blocks, {largest.size/1024} Kb) ***",
        )
        for l in largest.traceback.format():
            print(l)

    # def profiler_return_largest(self, episode):
    #     import tracemalloc
    #     # pick the last saved snapshot, filter noise
    #     snapshot = self.snaps[-1].filter_traces((
    #             tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
    #             tracemalloc.Filter(False, "<frozen importlib._bootstrap_external>"),
    #             tracemalloc.Filter(False, "<unknown>"),
    #     ))
    #     largest = snapshot.statistics("traceback")[0]

    #    return largest.size/1024

    def profiler_reboot(self):
        self.snaps.clear()
