#!/usr/bin/env python
#  type: ignore

from hands_on_rl_navigation.env.mpi_env.mpi_env import MpiEnv
import exputils as eu
from tqdm import trange
from hands_on_rl_navigation.networks import *
from hands_on_rl_navigation.networks.utils import *
from hands_on_rl_navigation.agent.sac.sac import *
from hands_on_rl_navigation.utils.plotter_utils import *


def policy_eval(config_run, **kwargs):
    seed = 5005
    config_env = eu.AttrDict(
        show_gui_for_testing=True,
        n_max_steps_per_epsiode=10,
        random_start_position=1,
        random_goal_position=1,
        n_round_tables=4,
        tables_positions=[[0, -3], [3, 0], [-3, 0], [0, 3]],
        probability_furnitures=1,  # probability to have furniture in the episode,
        draw_raycast=False,
    )
    # init of env
    env = MpiEnv(seed=seed, config=config_env)

    config_agent = eu.AttrDict(
        list_of_input_to_encode=[
            ["angle_to_goal_position", "distance_to_goal_position"],
            ["raycast"],
        ],
        list_of_input_to_memorize=[
            ["angle_to_goal_position", "distance_to_goal_position"],
            ["raycast"],
        ],
    )

    weights = eu.AttrDict(
        navigation_encoder="/local_scratch/victor/Documents/RobotLearn/Projects/drl_robot_navigation/drl-navigation-training/code/hands_on_rl_navigation/examples/train_example/experiments/experiment_00001/repetition_000000/checkpoints/final_nav_encoder_state_dict.pt",
        actor="/local_scratch/victor/Documents/RobotLearn/Projects/drl_robot_navigation/drl-navigation-training/code/hands_on_rl_navigation/examples/train_example/experiments/experiment_00001/repetition_000000/checkpoints/final_actor_state_dict.pt",
    )

    agent = SAC(env=env.config, import_weight=weights, config=config_agent)
    trajectories = []
    for episode in range(10):
        # set trajectory for each episode
        trajectory_per_episode = list()

        # env reset
        observation, info = env.reset()
        done = False
        score = 0

        # add position to trajectory list
        trajectory_per_episode.append(tuple(env.start_position))

        while not done:
            action = agent.predict_action(observation)
            # Step env with action
            observation, reward, terminated, truncated, info = env.step(action)
            trajectory_per_episode.append(tuple(env.agent.position))
            # update of parameters
            score += reward
            done = terminated or truncated

            if env.flag_goal_reached:
                print("The goal has been reached!")
            if env.flag_collision:
                print("There was a collision")
        print(score)
        # trajectory_per_episode.append(tuple(env.goal_position))
        trajectories.append(trajectory_per_episode)

    if config_run.trajectory_grid_show or config_run.save_plots:
        labels = [""]
        plot_single_policy_grid(
            env, info["global_static_grid"], trajectories, labels, config_run
        )
        # plot_multiple_policies_grid(env, info["global_static_grid"], trajectories, labels, config_run)
        # plot_multiple_policies_grid_with_same_start_ang_goal(env, info["global_static_grid"], trajectories, labels, config_run)

    env.close()


import os

if __name__ == "__main__":
    policy_eval(
        config_run=eu.AttrDict(trajectory_grid_show=True, n_episode=1, save_plots=False)
    )
