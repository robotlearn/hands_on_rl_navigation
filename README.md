# Tutorial on Deep Reinforcement Learning for Robot Navigation

Version 0.2.0 (13.02.2024)

# Description

This hands-on tutorial lets you train a Deep Reinforcement Learning (DRL) agent ([SAC](https://bair.berkeley.edu/blog/2018/12/14/sac/)) to navigate to a goal position while avoiding obstacles.
We will look at 3 important factors for DRL: how to tune 1) the reward function, 2) the agents' network architecture, and  3) the hyperparameters of the training.
The software for the tutorial contains a 2D simulation of the robot and its environment, the DRL agent with its training procedure, and tools to visualize the training results.


# Setup Instructions

##  1 - Downloading
- Create a code folder that will contains all the modules
- In the code folder, execute the following commands :
```shell
git clone https://gitlab.inria.fr/robotlearn/hands_on_rl_navigation.git
git clone https://gitlab.inria.fr/spring/wp6_robot_behavior/multiparty_interaction_simulator.git
```

## 2 - Installation of Miniconda3

Follow the respective links depending on your operating system :
* Linux Users : https://conda.io/projects/conda/en/latest/user-guide/install/linux.html
* MacOS Users : https://conda.io/projects/conda/en/latest/user-guide/install/macos.html
* Windows Users :https://conda.io/projects/conda/en/latest/user-guide/install/windows.html

Note for Windows Users : We highly recommend to install [WSL](https://learn.microsoft.com/fr-fr/windows/wsl/install) on your laptop to make things easier!

## 3 - Installation of Development Environment

Once conda is installed, open a Terminal in the code folder containing the cloned repository and execute the following command lines.
These commands create a conda environment and install all necessary packages to run the tutorial.

```shell
conda create --name hands_on_rl_navigation python=3.8
conda activate hands_on_rl_navigation
conda install pip
pip install -U pip
python3.8 -m pip install -e hands_on_rl_navigation
python3.8 -m pip install -e multiparty_interaction_simulator
```


## Test the Environment

Make sure the created conda environment is activated.
Run the file following command line :
```shell
cd ./hands_on_rl_navigation/examples/test_env/
python test_env.py
```

A window should open showing the 2D environment with randomly placed tables and a robot navigating with random actions.

# Training Experiments

The following experiments introduce three important aspects when it comes to use DRL for robot navigation.


## Exercise 1 : Reward Function

1 - Under `hands_on_rl_navigation/examples/train_example/experiments/` create a copy of the folder `experiment_00001/`. Under  `experiment_0000*/repetition_00000/` you need to code your own reward function based on the following formulas :

<img src="hands_on_rl_navigation/img/reward_function.png" max-width=10px>

<img src="hands_on_rl_navigation/img/reward_function_param.png" max-width=10px>

2 - Train the agent to see which reward function works the best.

3 - Visualize quantitative results with jupyter notebook file : examples/train_example/analyze/overview.ipynb

4 - Plot the policies with the plot function with plot.py.
Important note : Make sure to fill the respective path of the actor and navigation encoder network in line 41.

## Exercise 2 : Actor Network Architecture

1 - Under `hands_on_rl_navigation/examples/train_example/experiments/` create a copy of the folder `experiment_00001/`. Under  `experiment_0000*/repetition_00000/`  you can tune your own network architecture.

Important note : The last module on the nn.Sequential has to be a Relu activation function : nn.ReLU()

2 - Train the agent to see which architecture works the best.

3 - Visualize quantitative results with jupyter notebook file : examples/train_example/analyze/overview.ipynb.

4 - Plot the policies with the plot function with plot.py.
Important note : Make sure to fill the respective path of the actor and navigation encoder network in line 41.


## Exercise 3 : Hyperparameter Tunning

1 - Under `hands_on_rl_navigation/examples/train_example/experiments/` create a copy of the folder `experiment_00001/`. Under  `experiment_0000*/repetition_00000/` you can tune your own hyperparameters.

2 - Train the agent to see which hyperparameter works the best.

3 - Visualize quantitative results with jupyter notebook file : examples/train_example/analyze/overview.ipynb

4 - Plot the policies with the plot function with plot.py.
Important note : Make sure to fill the respective path of the actor and navigation encoder network in line 41.

## How to make several experiment for the same exercise.

1 - Under experiments folder, make a copy of the folder /experiment_00*00/.

2 - Modify the parameter you want of the respective run.py

3 - The folder architecture should look like this :
```shell
    hands_on_rl_navigation/
        examples/
            train_example/
                experiments/
                    experiment_00100/.
                    experiment_0010*/.
                    *
                    *
                    *
                    experiment_00200/.
                    experiment_0020*/.
                    *
                    *
                    *
                    experiment_00200/.
                    experiment_0020*/.

```
