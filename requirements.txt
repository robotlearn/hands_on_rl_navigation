# Requirements for RL
torch == 1.13.1
numpy == 1.22.4
gymnasium == 0.28.1
tensorboard == 2.13.0
torch == 1.13.1
matplotlib == 3.7.1
tqdm == 4.65.0
scikit-image == 0.21.0
Cython == 0.29.35
experiment-utilities == 0.3.6
opencv-python == 4.8.0.74
scikit-fmm == 2023.4.2